package ru.nix.bluetoothbutton.model;

import org.junit.Test;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.nix.bluetoothbutton.depricated.ButtonDeviceOLD;

import static org.junit.Assert.assertEquals;

public class MessageParserTest {

    @Test
    public void checkBattPattern() throws Exception{

        String mBuff = "ksjdfh$BAT,4.199V,099.99%kdjvnkejv  ijen kejng $BAT,4.234V,048.25%sdf";
        String COMMAND_BATTERY = "([$]BAT,)(\\d{1})(.)(\\d{3})(V,)(\\d{3})(.)(\\d{2})(%)";

        Pattern pattern;
        Matcher matcher;


        pattern = Pattern.compile("([$]BAT,)(\\d{1})(.)(\\d{3})(V,)(\\d{3})(.)(\\d{2})(%)");
        matcher = pattern.matcher(mBuff);

        assertEquals(true,matcher.find());
        assertEquals(4.199,
        Float.valueOf(matcher.group(2)+matcher.group(3)+matcher.group(4)).floatValue(), 0.001);
        assertEquals(99.99,
                Float.valueOf(matcher.group(6)+matcher.group(7)+matcher.group(8)).floatValue(), 0.001);

        assertEquals(true,matcher.find());
        assertEquals(4.234,
                Float.valueOf(matcher.group(2)+matcher.group(3)+matcher.group(4)).floatValue(), 0.001);
        assertEquals(48.25,
                Float.valueOf(matcher.group(6)+matcher.group(7)+matcher.group(8)).floatValue(), 0.001);

        assertEquals(false,matcher.find());


    }

    @Test
    public void checkButtonDown() throws Exception{
        ButtonMessage.MessageParser parser = new ButtonMessage.MessageParser();
        String msg="hdf$BUTTON_DOWN,5";
        parser.writeBytes(msg);
        List<ButtonMessage> newMessages;
        newMessages = parser.getNewMessages();
        assertEquals(5,newMessages.get(0).getButtonNumber());

        msg="hdf$BUTTON_DOWN,1sdf$df$BUTTON_DOWN,2df$BUTTON_DOWN,3df$BUTTON_DOWN,4df$BUTTON_DOWN,5df$BUTTON_DOWN,6";
        parser.writeBytes(msg);
        newMessages = parser.getNewMessages();
        assertEquals(6,newMessages.size());
    }

    @Test
    public void checkBatt() throws Exception{
        ButtonMessage.MessageParser parser = new ButtonMessage.MessageParser();
        String msg="hdf$BAT,4.254V,087.45%lwkej";
        parser.writeBytes(msg);
        List<ButtonMessage> newMessages;
        newMessages = parser.getNewMessages();
        assertEquals(1,newMessages.size());
        assertEquals(ButtonMessage.MessageType.BATTERY_LEVEL, newMessages.get(0).getMessageType());
        assertEquals(4.254,newMessages.get(0).getVoltage(),0.0001);
        assertEquals(87.45,newMessages.get(0).getPercentage(),0.0001);
    }

    @Test
    public void checkInactiveTimeout() throws Exception{
        ButtonMessage.MessageParser parser = new ButtonMessage.MessageParser();
        String msg="hdf$INACTIVE_TIMEOUT%lwkej";
        parser.writeBytes(msg);
        List<ButtonMessage> newMessages;
        newMessages = parser.getNewMessages();
        assertEquals(1,newMessages.size());
        assertEquals(ButtonMessage.MessageType.INACTIVE_TIMEOUT, newMessages.get(0).getMessageType());

    }


}
