package ru.nix.bluetoothbutton;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import ru.nix.bluetoothbutton.db.LogTable;
import ru.nix.bluetoothbutton.depricated.LogContentProvider;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("ru.nix.bluetoothbutton", appContext.getPackageName());
    }


    @Test
    public void useContentProvider() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        ContentResolver resolver = appContext.getContentResolver();
        Cursor cursor = resolver.query(LogContentProvider.LOG_URI, null,null,null,null);
        assertTrue(cursor.getColumnCount()>0);




        assertEquals("ru.nix.bluetoothbutton", appContext.getPackageName());
    }

    @Test
    public void addLogData() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        ContentResolver resolver = appContext.getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put(LogTable.COLUMN_LOG_DATE, System.currentTimeMillis());
        contentValues.put(LogTable.COlUMN_LOG_ACTION, "Test Action");
        Uri uri = resolver.insert(LogContentProvider.LOG_URI,contentValues);
        assertNotNull(uri);
    }

    @Test
    public void startSync() throws Exception {


    }
}
