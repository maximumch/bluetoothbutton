package ru.nix.bluetoothbutton.dao;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.nix.bluetoothbutton.model.ButtonPress;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(AndroidJUnit4.class)
public class ButtonPressesTest {
    ButtonDatabase db;
    ButtonPressStore store;
    @Before
    public void setUp() {
        db=ButtonDatabase.create(InstrumentationRegistry.getTargetContext(), true);
        store=db.buttonPressStore();
    }
    @After
    public void tearDown() {
        db.close();
    }



    @Test
    public void useAppContext() throws Exception {
        assertEquals(0,store.selectAllLD().getValue().size()); //Изначально база пуста
        ButtonPress press = new ButtonPress(1);
        assertNotNull(press.id);
        assertNotNull(press.date);
        assertNotEquals(0, press.id.length());
        store.insert(press);
        assertEquals(1,store.selectAllLD().getValue().size());

        ButtonPress extracted = store.selectAllLD().getValue().get(0);

        assertEquals(0,press.date.compareTo(extracted.date));

        store.delete(press);
        assertEquals(0,store.selectAllLD().getValue().size());



    }


}
