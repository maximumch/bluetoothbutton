package ru.nix.bluetoothbutton;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import ru.nix.bluetoothbutton.dao.ButtonDatabase;
import ru.nix.bluetoothbutton.model.ButtonAction;

public class ActionActivity extends AppCompatActivity {
    public static final String EXTRA_BUTTON_ID = "ru.nix.bluetoothbutton.actionactivity.extra_button_id";
    private State state;
    private TextView buttonText;
    private EditText buttonAction;
    private CheckBox cancelPrevious;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        buttonText = findViewById(R.id.button_text);
        buttonAction = findViewById(R.id.action_text);
        cancelPrevious = findViewById(R.id.cancel_previous);

        cancelPrevious.setOnCheckedChangeListener((compoundButton, checked) -> {
            buttonAction.setEnabled(!checked);
        });

        state=(State)getLastCustomNonConfigurationInstance();
        if(state==null) {
            state = new State();

        }
        Intent intent = getIntent();
        state.buttonId = intent.getIntExtra(EXTRA_BUTTON_ID,1);
        ButtonDatabase.get(this).buttonPressStore().selectActionLD(state.buttonId).observe(this, (actions) -> onChanged(actions));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                saveData();
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    private void saveAction(){
//        state.buttonAction.action = buttonAction.getText().toString();
//        saveData();
//    }
//
//    private void saveCancelPrevious(){
//        state.buttonAction.cancelPrevious = cancelPrevious.isChecked();
//        saveData();
//    }

    private void saveData() {
        if(state.buttonAction!=null) {
            if(buttonAction.getText().length()>0){
                state.buttonAction.action = buttonAction.getText().toString();
            }else{
                state.buttonAction.action = null;
            }
            state.buttonAction.cancelPrevious = cancelPrevious.isChecked();
            ButtonDatabase.get(this).buttonPressStore().updateButtonAction(state.buttonAction);
        }
    }

    private void onChanged(List<ButtonAction> actions) {
            if(state.buttonAction==null || !state.buttonAction.equals(actions.get(0))) {
                state.buttonAction = actions.get(0);

                String buttonTextString = getString(R.string.button1_name_prefix) + " " + actions.get(0).buttonId;
                buttonText.setText(buttonTextString);




                if (actions.get(0).cancelPrevious) {
                    cancelPrevious.setChecked(true);
                    buttonAction.setEnabled(false);
                } else {
                    cancelPrevious.setChecked(false);
                    buttonAction.setEnabled(true);
                }

                if (actions.get(0).action != null) {
                    buttonAction.setText(actions.get(0).action);
                } else {
                    buttonAction.setText(buttonTextString);
                }
            }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return(state);
    }

    private static class State{
        public int buttonId;
        public ButtonAction buttonAction;

    }

}
