package ru.nix.bluetoothbutton;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import ru.nix.bluetoothbutton.db.LogHelper;
import ru.nix.bluetoothbutton.db.LogTable;

/**
 * Created by Chernyshev on 15.06.18.
 */

public class SaveToFileService extends IntentService {
    Cursor cursor;
    LogHelper helper;

    public SaveToFileService() {
        super(".SaveToFileService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        helper = new LogHelper(this);
        SQLiteDatabase database = helper.getReadableDatabase();
        cursor = database.query(LogTable.TABLE_LOG
                , new String[]{LogTable.COLUMN_LOG_ID, LogTable.COLUMN_LOG_DATE, LogTable.COlUMN_LOG_ACTION}
                ,null,null,null,null
                , LogTable.COLUMN_LOG_DATE
                , null);

        String fileName="";
        try{
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format((new GregorianCalendar()).getTime());
            fileName="button_log_"+ timeStamp + ".txt";
            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + fileName;
            File file = new File(path);
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            String stringData;

            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                stringData = "<item d=\"" + String.valueOf(cursor.getString(1)) + "\" a=\"" + String.valueOf(cursor.getString(2)) + "\" />\n";
                outputStream.write(stringData.getBytes());
                cursor.moveToNext();
            }

            outputStream.close();


        }catch(Exception e){
            e.printStackTrace();

        }









    }
}
