package ru.nix.bluetoothbutton;




import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.arch.lifecycle.LifecycleService;
import android.arch.lifecycle.MutableLiveData;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncRequest;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import ru.nix.bluetoothbutton.dao.ButtonDatabase;
import ru.nix.bluetoothbutton.dao.ButtonPressStore;
import ru.nix.bluetoothbutton.model.ButtonDevice;
import ru.nix.bluetoothbutton.model.ButtonMessage;
import ru.nix.bluetoothbutton.model.ButtonPress;
import ru.nix.bluetoothbutton.model.ConnectionState;
import ru.nix.bluetoothbutton.model.DeviceLog;
import ru.nix.bluetoothbutton.model.RequestForSync;

import static android.bluetooth.BluetoothProfile.GATT;

public class BluetoothService extends LifecycleService {
    private static final byte[] MESSAGE_OK = {0x24,0x4F,0x4B,0x0D,0x0A}; //{0x4F,0x4B,0x0D,0x0A} ;// {0x24,0x4F,0x4B,0x0D,0x0A} ;
    private static final byte[] MESSAGE_BAT = {0x24,  0x42,0x41,0x54,0x0D,0x0A};
    private static final byte[] MESSAGE_INFO = {0x24,  0x49,0x4E,0x46,0x4F,   0x0D,0x0A};
    public static final int ONGOING_NOTIFICATION_ID = 2808;
    public static final String sUUID = "0000ffe0-0000-1000-8000-00805f9b34fb";
    public static final String cUUID = "0000ffe1-0000-1000-8000-00805f9b34fb";
    public static final String EXTRA_ACTION = "ru.nix.bluetoothbutton.bluetoothservice.extra_action";
    public static final String EXTRA_DEVICE = "ru.nix.bluetoothbutton.bluetoothservice.extra_device";
    public static final String ACTION_INITIAL_START = "ru.nix.bluetoothbutton.bluetoothservice.initial_start";
    public static final String ACTION_CONNECT = "ru.nix.bluetoothbutton.bluetoothservice.connect";
    public static final String ACTION_START_SCAN = "ru.nix.bluetoothbutton.bluetoothservice.start_scan";
    public static final String ACTION_DISCONNECT = "ru.nix.bluetoothbutton.bluetoothservice.disconnect";
    private UUID supportedUUID=UUID.fromString(sUUID);
    private UUID characteristicsUUID=UUID.fromString(cUUID);
    public static final String ACCOUNT_TYPE = "ru.nix.nixpassport";

    static final MutableLiveData<Event> BUSDevices =new MutableLiveData<>();

    private Notification notification;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mScanner;
    private BluetoothGatt gatt;
    private MScanCallback mScanCallback;
    private GattCallback bluetoothGattCallback= new GattCallback();
    private ButtonMessage.MessageParser messageParser;
    private List<BluetoothDevice> devicesDiscovered;
    private boolean scan = false;
    private boolean connecting = false;
    private boolean connected = false;


    private BluetoothManager bluetoothManager;
    private ButtonPressStore store;
    private ButtonDatabase db;
    private ScheduledExecutorService batteryRequestScheduleService;
    private ScheduledExecutorService connectionWatchDogScheduleService;




    public BluetoothService() {

        messageParser = new ButtonMessage.MessageParser();
        mScanCallback = new MScanCallback();
        devicesDiscovered = new ArrayList<>();

//        buttonDevicesDiscovered = new ArrayList<>();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        initNotification();
        initBluetoothAdapter();
        initStore();

        String action = intent.getStringExtra(EXTRA_ACTION);
        if (action != null && ACTION_INITIAL_START.equals(action)) {

        } else if (action != null && ACTION_CONNECT.equals(action)) {
            String address = intent.getStringExtra(BluetoothService.EXTRA_DEVICE);
            connect(address);
        }else if (action != null && ACTION_DISCONNECT.equals(action)) {
            disconnect();
        }else if (action != null && ACTION_START_SCAN.equals(action)) {
            if (!scan) {
                startScan();
            }
        } else {

        }
        return Service.START_NOT_STICKY;
    }

    private void onSyncRequest(List<RequestForSync> syncRequests) {
        if(syncRequests!=null && syncRequests.size()>0){
            runSync();
            store.removeSyncRequests(syncRequests);
        }

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        super.onBind(intent);
        return null;
    }

    private void initNotification(){
        if (notification == null) {
            Intent intent = new Intent(this, Main2Activity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_NO_CREATE );
            NotificationCompat.Builder b = new NotificationCompat.Builder(this);
            b.setSmallIcon(R.drawable.ic_keyboard_24dp)
                    .setContentTitle(getString(R.string.notification_title))
                    .setContentText(getString(R.string.notification_text))
                    .setContentIntent(pendingIntent);

            notification = b.build();
            startForeground(ONGOING_NOTIFICATION_ID, notification);
        }
    }

    private void initBluetoothAdapter(){
        if (mBluetoothAdapter == null) {
            bluetoothManager =
                    (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = bluetoothManager.getAdapter();
        }
    }

    private void initStore(){
        if(db==null){
            db = ButtonDatabase.get(this);
            store=db.buttonPressStore();
            store.setDisconnected();
            store.selectAllSyncRequestsLD().observe(this,(syncRequests -> onSyncRequest(syncRequests)));
        }
    }


    public void startScan(){
        scan = true;
        BUSDevices.postValue(new Event(null, Event.EventType.SCAN_STARTED));
        store.clearCanConnect();
        if(!mBluetoothAdapter.isEnabled()) {


        }else {
            mScanner = mBluetoothAdapter.getBluetoothLeScanner();
            mScanner.startScan(mScanCallback);
            scheduleStopScan(20000);
        }
    }

    private void scheduleStopScan(int i) {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.schedule(
                new Runnable() {
                    @Override
                    public void run() {
                        stopScan();
                    }
                },
                i,
                TimeUnit.MILLISECONDS
        );
    }

    private void scheduleStopConnecting(int i) {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

        service.schedule(
                new Runnable() {
                    @Override
                    public void run() {
                        stopConnect();
                    }
                },
                i,
                TimeUnit.MILLISECONDS
        );
    }

    private void stopConnect() {
        if(connecting){
            disconnect();
        }

    }

    public void stopScan() {
        if(scan) {
            scan = false;
            BUSDevices.postValue(new Event(null, Event.EventType.SCAN_FINISHED));
            mScanner.stopScan(mScanCallback);

        }
    }

    public void connect(String address) {
        BluetoothDevice bluetoothDevice = null;
        for(BluetoothDevice cBluetoothDevice: devicesDiscovered){
            if(address.equals(cBluetoothDevice.getAddress())){
                bluetoothDevice = cBluetoothDevice;
                break;
            }
        }
        if(bluetoothDevice!= null) {
            stopScan();
            connecting = true;
            disconnect();
            store.setConnectionState(address, ConnectionState.CONNECTING);
            gatt = bluetoothDevice.connectGatt(this, true, bluetoothGattCallback);
            scheduleStopConnecting(10000);
        }
    }

    private void disconnect() {
        if(gatt != null ){
            gatt.disconnect();
            gatt.close();
            store.setDisconnected();
        }
    }



    private void onMessage(ButtonMessage message) {
        switch(message.getMessageType()){
            case BUTTON_DOWN:
                store.addButtonPress(new ButtonPress(message.getButtonNumber()));
                store.addDeviceLog(new DeviceLog(message.getMessageType().name() + " " + message.getButtonNumber()));
                break;
            case BATTERY_LEVEL:
                store.updateBatteryLevel(message.getPercentage());
                store.addDeviceLog(new DeviceLog(message.getMessageType().name() + " " + message.getPercentage()));
                break;
            case USB_CONNECTED:
                store.updateUsbConnected(true);
                store.addDeviceLog(new DeviceLog(message.getMessageType().name()));
                break;
            case USB_DISCONNECTED:
                store.updateUsbConnected(false);
                store.addDeviceLog(new DeviceLog(message.getMessageType().name()));
                break;
            case POWER_OFF:
                store.addDeviceLog(new DeviceLog(message.getMessageType().name()));
                break;
            case BUTTON_UP:
                break;
            case BATTERY_LOW:
                store.addDeviceLog(new DeviceLog(message.getMessageType().name()));
                break;
            case BLUETOOTH_OFF:
                store.addDeviceLog(new DeviceLog(message.getMessageType().name()));
                break;
            case CHARGING_STARTED:
                store.addDeviceLog(new DeviceLog(message.getMessageType().name()));
                break;
            case INACTIVE_TIMEOUT:
                store.addDeviceLog(new DeviceLog(message.getMessageType().name()));
                break;

            case CHARGING_FINISHED:
                store.addDeviceLog(new DeviceLog(message.getMessageType().name()));
                break;
            case INACTIVE:
                store.addDeviceLog(new DeviceLog(message.getMessageType().name() + " " + message.getPercentage()));
                break;

        }
    }

    private void incrementButtonStatistics(int buttonId){
       store.incrementButtonStatistics(buttonId);
    }




    private void runSync(){
        AccountManager accountManager = AccountManager.get(this);
        Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
        if(accounts!=null){
            for(Account account:accounts)
                try {
                    Bundle extras = new Bundle();
                    extras.putString("abracadabra", "abracadabra");
                    SyncRequest syncRequest = new SyncRequest.Builder()
                            .setSyncAdapter(account, StubContentProvider.AUTHORITY)
                            .syncOnce()
                            .setExtras(extras)
                            .build();

                    ContentResolver.setIsSyncable(account, StubContentProvider.AUTHORITY, 1);
                    ContentResolver.setSyncAutomatically(account, StubContentProvider.AUTHORITY, true);
                    ContentResolver.requestSync(syncRequest);


                } catch (Exception e) {

                }
        }
    }





    private class MScanCallback extends ScanCallback {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            BluetoothDevice bluetoothDevice = result.getDevice();
            store.addCanConnect(new ButtonDevice(bluetoothDevice));

            if(!devicesDiscovered.contains(bluetoothDevice)){
                devicesDiscovered.add(bluetoothDevice);
            }
        }
    }

    public static class Event{
        public enum EventType{DISCONNECTED, SCAN_STARTED, SCAN_FINISHED, DEVICE_DISCOVERED, CONNECTING_STARTED, CONNECTED, CONNECTING_FAILED}
        private String deviceAddress;
        private EventType type;

        public Event(String deviceAddress, EventType type) {
            this.deviceAddress = deviceAddress;
            this.type = type;
        }

        public EventType getType() {
            return type;
        }
    }

    private class GattCallback extends BluetoothGattCallback {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if(newState == BluetoothProfile.STATE_CONNECTED){
                connected = true;
                connecting = false;
                store.setConnectionState(gatt.getDevice().getAddress(),ConnectionState.CONNECTED);
                gatt.discoverServices();
            }

            if(newState == BluetoothProfile.STATE_DISCONNECTED){
                cancelScheduleBatteryRequest();
                cancelScheduleConnectionWatchDog();
                store.setConnectionState(gatt.getDevice().getAddress(),ConnectionState.DISCONNECTED);
                connected = false;
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

            String s=characteristic.getStringValue(0);
            Log.d("happyMessage",s);
            messageParser.writeBytes(s);
            List<ButtonMessage> newMessages = messageParser.getNewMessages();
            if(newMessages.size()>0){
                for(ButtonMessage message:newMessages) {
                    if(message.getMessageType()== ButtonMessage.MessageType.BUTTON_DOWN){
                        characteristic.setValue(MESSAGE_OK);
                        characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);


                        //gatt.beginReliableWrite();



                        Boolean d = gatt.writeCharacteristic(characteristic);
                        Log.d("happy", "Write result = " + d);

                    }
                    onMessage(message);
                }
            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {

            if(status==BluetoothGatt.GATT_SUCCESS){
                BluetoothGattCharacteristic characteristic = gatt
                        .getService(supportedUUID)
                        .getCharacteristic(characteristicsUUID);

                gatt.setCharacteristicNotification(characteristic, true);
                scheduleBatteryRequest(characteristic);
                scheduleConnectionWatchDog();


            }

        }
    }

    private void scheduleBatteryRequest(BluetoothGattCharacteristic characteristic) {
        batteryRequestScheduleService = Executors.newSingleThreadScheduledExecutor();
        batteryRequestScheduleService.scheduleAtFixedRate(() -> {
                    characteristic.setValue(MESSAGE_BAT);
                    characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                    gatt.writeCharacteristic(characteristic);
                    characteristic.setValue(MESSAGE_INFO);
                    characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                    gatt.writeCharacteristic(characteristic);
                }
        , 1, 60, TimeUnit.SECONDS);
    }

    private void cancelScheduleBatteryRequest(){
        if(batteryRequestScheduleService!=null)   batteryRequestScheduleService.shutdown();
    }

    private void scheduleConnectionWatchDog(){
        connectionWatchDogScheduleService = Executors.newSingleThreadScheduledExecutor();
        connectionWatchDogScheduleService.scheduleAtFixedRate(() ->{
            List<BluetoothDevice> connectedDevices = bluetoothManager.getConnectedDevices(GATT);
            if(connectedDevices!=null && connectedDevices.size()>0){
               return;
            }
            disconnect();

        },5,5, TimeUnit.SECONDS);
    }

    private void cancelScheduleConnectionWatchDog(){
        if(connectionWatchDogScheduleService!=null)   connectionWatchDogScheduleService.shutdown();
    }


}
