package ru.nix.bluetoothbutton.depricated;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.os.IBinder;

import ru.nix.bluetoothbutton.db.LogHelper;
import ru.nix.bluetoothbutton.db.LogTable;
import ru.nix.bluetoothbutton.depricated.ConnectionAdapter;
import ru.nix.bluetoothbutton.depricated.ConnectionAdapterListener;
import ru.nix.bluetoothbutton.depricated.LogContentProvider;

public class LogService extends Service implements ConnectionAdapterListener {
    private LogHelper helper;


    public LogService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        helper = new LogHelper(this);
        ConnectionAdapter.getInstance().registerListener(this);
        writeLog("LogService created");
        //ConnectionAdapter.getInstance().connect();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {

        ConnectionAdapter.getInstance().unregisterListener(this);
        super.onDestroy();
    }

    @Override
    public void onEvent(EventType type, int button) {

        String action= "";
        switch (type){
            case ButtonDiscovered:
                action = "ButtonDiscovered";
                break;
            case ScanningStarted:
                action = "ScanningStarted";
                break;
            case Connecting:
                action = "Connecting";
                break;
            case Connected:
                action = "Connected";
                break;
            case ButtonPressed:
                action = "ButtonPressed" + String.valueOf(button);
                ConnectionAdapter.getInstance().incrementN(button - 1);
                break;
            case ScanningStoped:
                action = "ScanningStopped";
                break;
            case BluetoothDisabled:
                action = "BluetoothDisabled";
                break;
            case Disconnected:
                action = "Disconnected";
                break;

        }
        writeLog(action);
        ConnectionAdapter.getInstance().saveNs(this);

    }

    private void writeLog(String action){
        ContentValues values = new ContentValues();
        values.put(LogTable.COLUMN_LOG_DATE,String.valueOf(System.currentTimeMillis()));
        values.put(LogTable.COlUMN_LOG_ACTION,action);
        ContentResolver resolver = getContentResolver();
        resolver.insert(LogContentProvider.LOG_URI,values);

    }
}
