package ru.nix.bluetoothbutton.depricated;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import ru.nix.bluetoothbutton.db.ButtonsTable;
import ru.nix.bluetoothbutton.db.LogHelper;
import ru.nix.bluetoothbutton.db.LogTable;

/**
 * Created by Chernyshev on 22.06.18.
 */

public class LogContentProvider extends ContentProvider {
    public static final String CONTENT_AUTHORITY = "ru.nix.bluetoothbutton.log";
    public static final Uri LOG_URI = Uri.parse("content://"+CONTENT_AUTHORITY + "/log");
    public static final Uri BUTTONS_URI = Uri.parse("content://"+CONTENT_AUTHORITY + "/buttons");


    private static final UriMatcher matcher;
    public static final int ALL_LOG_ROWS = 1;
    public static final int ALL_BUTTONS_ROWS = 2;
    public static final int SINGLE_BUTTONS_ROW = 3;
    static {
        matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(CONTENT_AUTHORITY, "log",ALL_LOG_ROWS);
        matcher.addURI(CONTENT_AUTHORITY, "buttons", ALL_BUTTONS_ROWS);
        matcher.addURI(CONTENT_AUTHORITY, "buttons/#", SINGLE_BUTTONS_ROW);
        // # - число
        // * - строка
    }

    private LogHelper helper;
    private SQLiteStatement insert_log;
    private SQLiteStatement insert_buttons;
    int rowLimit = 100;




    @Override
    public boolean onCreate() {
        //Инициализируем Helper
        helper = new LogHelper(getContext());
        //Прошло успешно
        SQLiteDatabase db = helper.getReadableDatabase();
        insert_log = db.compileStatement(LogTable.LOG_INSERT);
        insert_buttons = db.compileStatement(ButtonsTable.BUTTONS_INSERT);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri,
                        @Nullable String[] projection,
                        @Nullable String selection,
                        @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        switch (matcher.match(uri)){
            case SINGLE_BUTTONS_ROW:
                return querySingleButton(uri,projection,selection,selectionArgs,sortOrder);
            case ALL_LOG_ROWS:
                return queryAllLog(uri,projection,selection,selectionArgs,sortOrder);
            case ALL_BUTTONS_ROWS:
                return queryAllButtons(uri,projection,selection,selectionArgs,sortOrder);
            default:
                throw  new IllegalArgumentException("Unsupported uri: " + uri);
        }
    }

    private Cursor queryAllButtons(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    private Cursor queryAllLog(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(
                LogTable.TABLE_LOG,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
        cursor.setNotificationUri(getContext().getContentResolver(),LOG_URI);
        return cursor;
    }

    private Cursor querySingleButton(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }


    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (matcher.match(uri)){
            case SINGLE_BUTTONS_ROW:
                return "vnd.android.cursor.item/vnd.buttons";
            case ALL_LOG_ROWS:
                return "vnd.android.cursor.dir/vnd.log";
            case ALL_BUTTONS_ROWS:
                return "vnd.android.cursor.dir/vnd.buttons";
            default:
                throw  new IllegalArgumentException("Unsupported uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        switch (matcher.match(uri)){
            case ALL_LOG_ROWS:
                return insertLog(uri, contentValues);
            case ALL_BUTTONS_ROWS:
                return insertButtons(uri, contentValues);
            default:
                throw  new IllegalArgumentException("Unsupported uri: " + uri);
        }
    }

    private Uri insertButtons(Uri uri, ContentValues contentValues) {
        return null;
    }

    private Uri insertLog(Uri uri, ContentValues contentValues) {
        SQLiteDatabase db = helper.getWritableDatabase();
        long id = db.insert(LogTable.TABLE_LOG,null,contentValues);
        if(id>=0){
            Uri inserted = ContentUris.withAppendedId(LOG_URI,id);
            getContext().getContentResolver().notifyChange(uri,null);
            return inserted;
        }
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
