package ru.nix.bluetoothbutton.depricated;

import android.bluetooth.BluetoothDevice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.nix.bluetoothbutton.model.ButtonMessage;

/**
 * Created by Chernyshev on 24.04.18.
 */

public class ButtonDeviceOLD {
    private String address;
    private String name;
    private long lastFound;
    private BluetoothDevice bluetoothDevice;


    public ButtonDeviceOLD(String address, String name) {
        this.address = address;
        this.name = name;
    }

    public ButtonDeviceOLD(String address, String name, long lastFound, BluetoothDevice bluetoothDevice) {
        this.address = address;
        this.name = name;
        this.lastFound = lastFound;
        this.bluetoothDevice = bluetoothDevice;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public void setLastFound(long lastFound) {
        this.lastFound = lastFound;
    }

    public long getLastFound() {
        return lastFound;
    }

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    /**
     * Created by Chernyshev on 11.05.18.
     */

    public static class MessageParser { //$BUTTON_DOWN,2
        static final String COMMAND_BUTTON_DOWN = "([$]BUTTON_DOWN,)(\\d{1})";
        static final int[] COMMAND_BUTTON_DOWN_NUMBER_GROUPS = {2};
        static final String COMMAND_BUTTON_UP = "([$]BUTTON_UP,)(\\d{1})";
        static final int[] COMMAND_BUTTON_UP_NUMBER_GROUPS = {2};
        static final String COMMAND_BATTERY = "([$]BAT,)(\\d{1})(.)(\\d{3})(V,)(\\d{3})(.)(\\d{2})(%)";
        static final int[] COMMAND_BATTERY_VOLTAGE_GROUPS = {2,3,4};
        static final int[] COMMAND_BATTERY_PERCENTAGE_GROUPS = {6,7,8};



        private String mBuff = "";
        private List<ButtonMessage> newMessages = new ArrayList<>();

        public synchronized void writeBytes(String str){
            if(str==null) return;
            mBuff += str;
            parseMessages();
        }

        public List<ButtonMessage> getNewMessages() {
            List<ButtonMessage> result = newMessages;
            newMessages = new ArrayList<>();
            return result;
        }

        private void parseMessages(){
            Pattern pattern;
            Matcher matcher;
            int position;

            ButtonMessage message;
            ArrayList<Integer> positions = new ArrayList<>();

            pattern = Pattern.compile(COMMAND_BUTTON_DOWN);
            matcher = pattern.matcher(mBuff);
            while(matcher.find()){
                int buttonNumber;
                String sButtonNumber = matcher.group(2);
                buttonNumber = Integer.parseInt(sButtonNumber);
                message = new ButtonMessage(GregorianCalendar.getInstance().getTime(),buttonNumber, ButtonMessage.MessageType.BUTTON_DOWN);
                newMessages.add(message);
                position = matcher.end(matcher.groupCount());
                positions.add(position);
            }

            if(positions.size()>0) {
                int maxPosition = Collections.max(positions);
                mBuff = mBuff.substring(maxPosition);
            }
        }

        private void parseMessagesOfType(String commandPattern, ButtonMessage.MessageType messageType){
            Pattern pattern;
            Matcher matcher;
            pattern = Pattern.compile(commandPattern);
            matcher = pattern.matcher(mBuff);



        }




    }
}
