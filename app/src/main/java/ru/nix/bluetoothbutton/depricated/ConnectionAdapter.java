package ru.nix.bluetoothbutton.depricated;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import ru.nix.bluetoothbutton.model.ButtonMessage;

import static ru.nix.bluetoothbutton.depricated.ConnectionAdapterListener.*;
import static ru.nix.bluetoothbutton.depricated.ConnectionAdapterListener.EventType.ButtonDiscovered;

public class ConnectionAdapter  {
    public static final String SHARED_PREFERENCES_PRESSES_NUMBER = "press_number";
    public static final String SHARED_PREFERENCES_BUTTON1 = "button1";
    public static final String SHARED_PREFERENCES_BUTTON2 = "button2";
    public static final String SHARED_PREFERENCES_BUTTON3 = "button3";
    public static final String SHARED_PREFERENCES_BUTTON4 = "button4";
    public static final String SHARED_PREFERENCES_BUTTON5 = "button5";
    public static final String SHARED_PREFERENCES_BUTTON6 = "button6";

    private static final byte[] MESSAGE_OK = {0x4F,0x4B,0x0D,0x0A} ;
    private static final int BUTTON_PRESSED = 1 ;
    private static final int BUTTON_CONNECTED = 2 ;
    private static final int BUTTON_DISCONNECTED = 3 ;
    private static ConnectionAdapter mInstance;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mScanner;
    private MScanCallback mScanCallback;
    private ArrayList<ScanResult> results;
    private ArrayList<WeakReference<ConnectionAdapterListener>> listeners;
    private static final String sUUID = "0000ffe0-0000-1000-8000-00805f9b34fb";
    private static final String cUUID = "0000ffe1-0000-1000-8000-00805f9b34fb";
    private UUID supportedUUID=UUID.fromString(sUUID);
    private UUID characteristicsUUID=UUID.fromString(cUUID);
    private State state = State.Disconnected;
    private BluetoothGatt bluetoothGatt;
    private BluetoothGattCallback bluetoothGattCallback;
    private ButtonDeviceOLD.MessageParser messageParser;
    private int[] ns = {0,0,0,0,0,0};

    public enum State{Scanning, Connecting, Connected, Disconnected, BlueToothDisabled}

    private List<ButtonDeviceOLD> buttonDevices;

    private ConnectionAdapter(){
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        results = new ArrayList<ScanResult>();
        listeners = new ArrayList<>();
        buttonDevices = new ArrayList<ButtonDeviceOLD>();
        mScanCallback = new MScanCallback();
        messageParser = new ButtonDeviceOLD.MessageParser();
    }

    public void restoreNs(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_PRESSES_NUMBER,Context.MODE_PRIVATE);
        ns[0] = sharedPreferences.getInt(SHARED_PREFERENCES_BUTTON1,0);
        ns[1] = sharedPreferences.getInt(SHARED_PREFERENCES_BUTTON2,0);
        ns[2] = sharedPreferences.getInt(SHARED_PREFERENCES_BUTTON3,0);
        ns[3] = sharedPreferences.getInt(SHARED_PREFERENCES_BUTTON4,0);
        ns[4] = sharedPreferences.getInt(SHARED_PREFERENCES_BUTTON5,0);
        ns[5] = sharedPreferences.getInt(SHARED_PREFERENCES_BUTTON6,0);
    }

    public void saveNs(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_PRESSES_NUMBER,Context.MODE_PRIVATE);
        sharedPreferences.edit()
                .putInt(SHARED_PREFERENCES_BUTTON1, ns[0])
                .putInt(SHARED_PREFERENCES_BUTTON2, ns[1])
                .putInt(SHARED_PREFERENCES_BUTTON3, ns[2])
                .putInt(SHARED_PREFERENCES_BUTTON4, ns[3])
                .putInt(SHARED_PREFERENCES_BUTTON5, ns[4])
                .putInt(SHARED_PREFERENCES_BUTTON6, ns[5])
                .apply();
    }

    public void setN(int index, int val){
        ns[index] = val;
    }

    public int getN(int index){
        return ns[index];
    }

    public void incrementN(int index){
        ns[index]++;
    }


    public static ConnectionAdapter getInstance() {
        if(mInstance==null){
            mInstance = new ConnectionAdapter();
        }
        return mInstance;
    }

    public synchronized void registerListener(ConnectionAdapterListener listener){
        unregisterListener(listener);
        listeners.add(new WeakReference<>(listener));
    }

    public void unregisterListener(ConnectionAdapterListener listener){
        for(WeakReference<ConnectionAdapterListener> weakListener:listeners){
            ConnectionAdapterListener cListener = weakListener.get();
            if(cListener!=null && cListener == listener){
                listeners.remove(weakListener);
                return;
            }
        }
    }

    public void startScan(){
        state= State.Scanning;
        buttonDevices.clear();
        sendEventToListeners(EventType.ScanningStarted, 0);

        if(!mBluetoothAdapter.isEnabled()) {
            state = State.BlueToothDisabled;
            sendEventToListeners(EventType.BluetoothDisabled, 0);

        }else {
            mScanner = mBluetoothAdapter.getBluetoothLeScanner();
            mScanner.startScan(mScanCallback);
        }
    }

    public void stopScan() {
        if(state==State.Scanning) {
            state = State.Disconnected;
            sendEventToListeners(EventType.ScanningStoped, 0);
            mScanner.stopScan(mScanCallback);
        }
    }

    public void connect(Context context, ButtonDeviceOLD button) {
        if(state!=State.Connecting) {
            stopScan();
            state = State.Connecting;
            sendEventToListeners(EventType.Connecting, 0);
            bluetoothGattCallback= new GattCallback();
            bluetoothGatt = button.getBluetoothDevice().connectGatt(context,true,bluetoothGattCallback );
        }
    }




    private class MScanCallback extends ScanCallback{
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            BluetoothDevice bluetoothDevice =result.getDevice();
            String address= bluetoothDevice.getAddress();
            String name = bluetoothDevice.getName();


            long curDate = (new GregorianCalendar()).getTimeInMillis();
            boolean exists = false;



            for(ButtonDeviceOLD device: buttonDevices){
               if(device.getAddress().equals(address)){
                   device.setLastFound(curDate);
                   exists = true;
               }
            }

            if(!exists){
                buttonDevices.add(new ButtonDeviceOLD(address,name,curDate,bluetoothDevice));
                sendEventToListeners(ButtonDiscovered, 0 );
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {

        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.v("ScanCallback", "onScanFailed");
        }


    }

    public List<ButtonDeviceOLD> getButtonDevices() {
        return buttonDevices;
    }

    public State getState() {
        return state;
    }

    private class GattCallback extends  BluetoothGattCallback{
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
           // super.onConnectionStateChange(gatt, status, newState);
            if(newState == BluetoothProfile.STATE_CONNECTED){
                state = State.Connected;
                Message readMsg = mHandler.obtainMessage(BUTTON_CONNECTED,0,-1);
                readMsg.sendToTarget();
                gatt.discoverServices();
            }

            if(newState == BluetoothProfile.STATE_DISCONNECTED){
                state = State.Disconnected;
                Message readMsg = mHandler.obtainMessage(BUTTON_DISCONNECTED,0,-1);
                readMsg.sendToTarget();

            }


        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            String s=characteristic.getStringValue(0);
            messageParser.writeBytes(s);
            List<ButtonMessage> newMessages = messageParser.getNewMessages();
            if(newMessages.size()>0){
                for(ButtonMessage message:newMessages) {
                    characteristic.setValue(MESSAGE_OK);
                    gatt.writeCharacteristic(characteristic);
                    Message readMsg = mHandler.obtainMessage(BUTTON_PRESSED,0,-1,message);
                    readMsg.sendToTarget();
                }
            }
            Log.d("happy",s);
           }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {

            if(status==BluetoothGatt.GATT_SUCCESS){
                BluetoothGattCharacteristic characteristic = gatt
                        .getService(supportedUUID)
                        .getCharacteristic(characteristicsUUID);

                gatt.setCharacteristicNotification(characteristic, true);


            }
            super.onServicesDiscovered(gatt, status);
        }
    }

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg){

            int numBytes;
            ButtonMessage  mBuffer = (ButtonMessage) msg.obj;

            if(msg.what==BUTTON_PRESSED) {
                onButtonPressed(mBuffer);
            }
            if(msg.what==BUTTON_CONNECTED) {
                onButtonConnected();
            } if(msg.what==BUTTON_DISCONNECTED) {
                onButtonDisonnected();
            }

        }
    };

    private void onButtonDisonnected() {
        sendEventToListeners(EventType.Disconnected, 0);
    }

    private void onButtonConnected() {
        sendEventToListeners(EventType.Connected, 0);
    }

    private void onButtonPressed(ButtonMessage message) {
        sendEventToListeners(EventType.ButtonPressed, message.getButtonNumber());
    }

    private void sendEventToListeners(EventType eventType, int button){
        for(WeakReference<ConnectionAdapterListener> weakListener: listeners) {
            ConnectionAdapterListener mListener =  weakListener.get();
            if(mListener!=null) {
                mListener.onEvent(eventType, button);
            }
        }
    }


}
