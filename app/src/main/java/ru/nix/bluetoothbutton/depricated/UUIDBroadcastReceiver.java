package ru.nix.bluetoothbutton.depricated;

import android.arch.lifecycle.MutableLiveData;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.util.Log;

import java.util.UUID;

import ru.nix.bluetoothbutton.BluetoothService;

public class UUIDBroadcastReceiver extends BroadcastReceiver {
    static final MutableLiveData<BluetoothDevice> UUIDBluetoothDevices =new MutableLiveData<>();


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("happy", "onReceive " );
        BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        Parcelable[] parcelables = intent.getParcelableArrayExtra(BluetoothDevice.EXTRA_UUID);
        ParcelUuid[] parcelUuids = bluetoothDevice.getUuids();
        if(UUIDBluetoothDevices.hasActiveObservers()) {

            if(parcelUuids!=null) {
                UUID serviceUUID = UUID.fromString(BluetoothService.sUUID);
                for (ParcelUuid parcelUuid : parcelUuids) {
                    if (parcelUuid.getUuid().compareTo(serviceUUID) == 0) {
                        UUIDBluetoothDevices.postValue(bluetoothDevice);
                        break;
                    }
                }
            }
        }


//        Intent serviceIntent = new Intent(context, BluetoothService.class);
//        serviceIntent.putExtras(intent.getExtras());
//        serviceIntent.putExtra(BluetoothService.EXTRA_ACTION, BluetoothService.ACTION_UUID_BROADCAST_RECEIVED );
//        context.startService(serviceIntent);
    }
}
