package ru.nix.bluetoothbutton.depricated;

import android.content.ContentResolver;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import ru.nix.bluetoothbutton.R;
import ru.nix.bluetoothbutton.db.LogHelper;
import ru.nix.bluetoothbutton.db.LogTable;
import ru.nix.bluetoothbutton.depricated.ConnectionAdapter;
import ru.nix.bluetoothbutton.depricated.ConnectionAdapterListener;
import ru.nix.bluetoothbutton.depricated.LogContentProvider;

public class LogViewActivity extends AppCompatActivity implements ConnectionAdapterListener {
    ListView list;
    SimpleCursorAdapter adapter;
    Cursor cursor;
    LogHelper helper;
    int rowLimit = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_view);
        list  = (ListView) findViewById(R.id.list);

        helper = new LogHelper(this);
        swapCursor();

    }

    private void swapCursor() {
        /*
        SQLiteDatabase database = helper.getReadableDatabase();
        cursor = database.query(LogTable.TABLE_LOG
                , new String[]{LogTable.COLUMN_LOG_ID, LogTable.COLUMN_LOG_DATE, LogTable.COlUMN_LOG_ACTION}
                ,null,null,null,null
                , LogTable.COLUMN_LOG_DATE + "  desc"
                , String.valueOf(rowLimit) );
                */

        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(LogContentProvider.LOG_URI, null,null,null,null);

        adapter = new SimpleCursorAdapter(this
                ,android.R.layout.simple_list_item_1
                ,cursor
                ,new String[]{LogTable.COlUMN_LOG_ACTION}
                ,new int[] {android.R.id.text1}
                ,0);
            list.setAdapter(adapter);


    }



    @Override
    protected void onResume() {
        ConnectionAdapter.getInstance().registerListener(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        ConnectionAdapter.getInstance().unregisterListener(this);
        super.onPause();
    }

    @Override
    public void onEvent(EventType type, int button) {
        swapCursor();


    }
}
