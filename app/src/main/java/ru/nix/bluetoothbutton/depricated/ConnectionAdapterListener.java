package ru.nix.bluetoothbutton.depricated;

import java.util.EventListener;
import java.util.List;

public interface ConnectionAdapterListener {
    abstract void onEvent(EventType type, int button);

    public enum EventType{Connected, ButtonPressed, BluetoothDisabled, ScanningStarted, ScanningStoped, Connecting, Disconnected, ButtonDiscovered}


}
