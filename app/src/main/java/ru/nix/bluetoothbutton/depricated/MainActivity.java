package ru.nix.bluetoothbutton.depricated;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import ru.nix.bluetoothbutton.R;
import ru.nix.bluetoothbutton.SaveToFileService;
import ru.nix.bluetoothbutton.UploadService;
import ru.nix.bluetoothbutton.depricated.ButtonDeviceOLD;
import ru.nix.bluetoothbutton.depricated.ButtonListAdapter;
import ru.nix.bluetoothbutton.depricated.ConnectionAdapter;
import ru.nix.bluetoothbutton.depricated.ConnectionAdapterListener;
import ru.nix.bluetoothbutton.depricated.LogViewActivity;

public class MainActivity extends AppCompatActivity implements ConnectionAdapterListener, AdapterView.OnItemClickListener {

    private TextView mTextView;
    private ListView buttonsList;
    private TextView status;
    private TextView tn1;
    private TextView tn2;
    private TextView tn3;
    private TextView tn4;
    private TextView tn5;
    private TextView tn6;
    private ButtonListAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tn1 = (TextView) findViewById(R.id.n1);
        tn2 = (TextView) findViewById(R.id.n2);
        tn3 = (TextView) findViewById(R.id.n3);
        tn4 = (TextView) findViewById(R.id.n4);
        tn5 = (TextView) findViewById(R.id.n5);
        tn6 = (TextView) findViewById(R.id.n6);

        status = (TextView) findViewById(R.id.status_text);

        buttonsList = (ListView) findViewById(R.id.buttons_list);
        //adapter = new ButtonListAdapter(ConnectionAdapter.getInstance().getButtonDevices());
        buttonsList.setAdapter(adapter);
        buttonsList.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ConnectionAdapter.getInstance().registerListener(this);
        updateButtonsN();
        setStatusText();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ConnectionAdapter.getInstance().unregisterListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.startScan:
                ConnectionAdapter.getInstance().startScan();
                return true;
            case R.id.startLogView:
                Intent intent = new Intent(this, LogViewActivity.class);
                startActivity(intent);
                return true;
            case R.id.saveLogToFile:
                Intent intent1 = new Intent(this, SaveToFileService.class);
                startService(intent1);
                return true;
            case R.id.uploadToServer:
                Intent intent2 = new Intent(this, UploadService.class);
                startService(intent2);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEvent(EventType type, int button) {
        switch (type){
            case ButtonDiscovered:
                buttonsList.setVisibility(View.VISIBLE);
                //adapter.setData(ConnectionAdapter.getInstance().getButtonDevices());
                adapter.notifyDataSetChanged();
                setStatusText();
                break;
            case ScanningStarted:
                setStatusText();
                break;
            case Connecting:
                setStatusText();
                break;
            case Connected:
                buttonsList.setVisibility(View.GONE);
                setStatusText();
                break;
            case ButtonPressed:

                updateButtonsN();

                break;
            case ScanningStoped:
                setStatusText();
                break;
            case BluetoothDisabled:
                setStatusText();
                break;
            case Disconnected:
                setStatusText();
                break;

        }


    }

    private void updateButtonsN() {
        try {
            tn1.setText(String.valueOf(ConnectionAdapter.getInstance().getN(0)));
            tn2.setText(String.valueOf(ConnectionAdapter.getInstance().getN(1)));
            tn3.setText(String.valueOf(ConnectionAdapter.getInstance().getN(2)));
            tn4.setText(String.valueOf(ConnectionAdapter.getInstance().getN(3)));
            tn5.setText(String.valueOf(ConnectionAdapter.getInstance().getN(4)));
            tn6.setText(String.valueOf(ConnectionAdapter.getInstance().getN(5)));
        }catch (Exception e){
            Log.d("happy", e.getMessage());

        }

    }

    private void setStatusText() {


        switch (ConnectionAdapter.getInstance().getState()) {
            case Scanning:
                status.setText(getString(R.string.status_scanning));
                break;
            case Connected:
                status.setText(getString(R.string.status_connected));
                break;
            case Connecting:
                status.setText(getString(R.string.status_connecting));
                break;
            case Disconnected:
                status.setText(getString(R.string.status_disconnected));
                break;
            case BlueToothDisabled:
                status.setText(getString(R.string.status_bluetooth_disabled));
        }


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ButtonDeviceOLD button= (ButtonDeviceOLD)   adapter.getItem(i);
        ConnectionAdapter.getInstance().connect(this, button);
    }
}
