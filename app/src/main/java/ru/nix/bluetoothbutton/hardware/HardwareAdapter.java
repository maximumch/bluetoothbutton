package ru.nix.bluetoothbutton.hardware;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import ru.nix.bluetoothbutton.depricated.ButtonDeviceOLD;
import ru.nix.bluetoothbutton.model.ButtonMessage;
import ru.nix.bluetoothbutton.depricated.ConnectionAdapter;
import ru.nix.bluetoothbutton.depricated.ConnectionAdapterListener;

public class HardwareAdapter {


    private static final byte[] MESSAGE_OK =  {0x24,0x4F,0x4B,0x0D,0x0A} ;
    private static final int BUTTON_PRESSED = 1 ;
    private static final int BUTTON_CONNECTED = 2 ;
    private static final int BUTTON_DISCONNECTED = 3 ;
    private static HardwareAdapter mInstance;
    private static Context mContext;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mScanner;
    private MScanCallback mScanCallback;
    private ArrayList<ScanResult> results;
    private ArrayList<WeakReference<ConnectionAdapterListener>> listeners;
    private static final String sUUID = "0000ffe0-0000-1000-8000-00805f9b34fb";
    private static final String cUUID = "0000ffe1-0000-1000-8000-00805f9b34fb";
    private UUID supportedUUID=UUID.fromString(sUUID);
    private UUID characteristicsUUID=UUID.fromString(cUUID);
    private ConnectionAdapter.State state = ConnectionAdapter.State.Disconnected;
    private BluetoothGatt bluetoothGatt;
    private BluetoothGattCallback bluetoothGattCallback;
    private ButtonDeviceOLD.MessageParser messageParser;
    private int[] ns = {0,0,0,0,0,0};

    public enum State{Scanning, Connecting, Connected, Disconnected, BlueToothDisabled}

    private List<ButtonDeviceOLD> buttonDevices;

    private HardwareAdapter(Context context){
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        messageParser = new ButtonDeviceOLD.MessageParser();
        mScanCallback = new MScanCallback();
        mContext = context;
        buttonDevices = new ArrayList<>();
    }

    public static synchronized void create(Context context){
        if(mInstance==null){
            mInstance = new HardwareAdapter(context);
        }
    }



    public static synchronized HardwareAdapter getInstance() {
        if(mInstance==null){
            throw new RuntimeException("HardwareAdapter instance does not exist");
        }
        return mInstance;
    }


    public void startScan(){
        Log.d("happy", "startScan1" );
        state= ConnectionAdapter.State.Scanning;
        buttonDevices.clear();
        //sendEventToListeners(ConnectionAdapterListener.EventType.ScanningStarted, 0);

        if(!mBluetoothAdapter.isEnabled()) {
            state = ConnectionAdapter.State.BlueToothDisabled;
            //sendEventToListeners(ConnectionAdapterListener.EventType.BluetoothDisabled, 0);

        }else {
            mScanner = mBluetoothAdapter.getBluetoothLeScanner();
            Log.d("happy", "startScan2" );
            mScanner.startScan(mScanCallback);
        }
    }

    public void stopScan() {
        Log.d("happy", "stopScan" );
        if(state== ConnectionAdapter.State.Scanning) {
            state = ConnectionAdapter.State.Disconnected;
            //sendEventToListeners(ConnectionAdapterListener.EventType.ScanningStoped, 0);
            mScanner.stopScan(mScanCallback);
        }
    }

    public void connect(ButtonDeviceOLD button) {
        Log.d("happy", "connect" );
        if(state!= ConnectionAdapter.State.Connecting) {
            stopScan();
            state = ConnectionAdapter.State.Connecting;
            //sendEventToListeners(ConnectionAdapterListener.EventType.Connecting, 0);
            bluetoothGattCallback= new GattCallback();
            bluetoothGatt = button.getBluetoothDevice().connectGatt(mContext,true,bluetoothGattCallback );

        }
    }




    private class MScanCallback extends ScanCallback {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            Log.d("happy", "onScanResult " + result.toString());
            Log.d("happy", "onScanResult " + result.getDevice().toString());
            Log.d("happy", "onScanResult " + result.getDevice().getName());
            BluetoothDevice bluetoothDevice =result.getDevice();
            String address= bluetoothDevice.getAddress();
            String name = bluetoothDevice.getName();
            Log.d("happy", "onScanResult " + name);

            long curDate = (new GregorianCalendar()).getTimeInMillis();
            boolean exists = false;



            for(ButtonDeviceOLD device: buttonDevices){
                if(device.getAddress().equals(address)){
                    device.setLastFound(curDate);
                    exists = true;
                }
            }

            if(!exists){
                //buttonDevices.add(new ButtonDevice(address,name,curDate,bluetoothDevice));
                //stopScan();
                connect(new ButtonDeviceOLD(address,name,curDate,bluetoothDevice));

                //sendEventToListeners(ButtonDiscovered, 0 );
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            Log.d("happy", "onBatchScanResults " );
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.d("happy", "onScanFailed " );
        }


    }



    private class GattCallback extends  BluetoothGattCallback{
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d("happy", "onConnectionStateChange status=" + status + " newState=" + newState );
            // super.onConnectionStateChange(gatt, status, newState);
            if(newState == BluetoothProfile.STATE_CONNECTED){
                state = ConnectionAdapter.State.Connected;
                gatt.discoverServices();
            }

            if(newState == BluetoothProfile.STATE_DISCONNECTED){
                state = ConnectionAdapter.State.Disconnected;

            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            Log.d("happy", "onCharacteristicRead" );
        }



        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

            Log.d("happy", "onCharacteristicChanged" );
            String s=characteristic.getStringValue(0);
            messageParser.writeBytes(s);
            List<ButtonMessage> newMessages = messageParser.getNewMessages();
            if(newMessages.size()>0){
                for(ButtonMessage message:newMessages) {
                    characteristic.setValue(MESSAGE_OK);
                    gatt.writeCharacteristic(characteristic);


                }
            }
            Log.d("happy","value = " + s);
            super.onCharacteristicChanged(gatt, characteristic);
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
            Log.d("happy","onDescriptorRead " + descriptor.toString() );
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            Log.d("happy","onReadRemoteRssi");
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.d("happy","onServicesDiscovered");
            if(status==BluetoothGatt.GATT_SUCCESS){
                BluetoothGattCharacteristic characteristic = gatt
                        .getService(supportedUUID)
                        .getCharacteristic(characteristicsUUID);

                gatt.setCharacteristicNotification(characteristic, true);
           }
            super.onServicesDiscovered(gatt, status);
        }
    }







}
