package ru.nix.bluetoothbutton.dao;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.util.Log;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.nix.bluetoothbutton.model.ButtonAction;
import ru.nix.bluetoothbutton.model.ButtonActionLog;
import ru.nix.bluetoothbutton.model.ButtonDevice;
import ru.nix.bluetoothbutton.model.ButtonPress;
import ru.nix.bluetoothbutton.model.ButtonStatistics;
import ru.nix.bluetoothbutton.model.ConnectionState;
import ru.nix.bluetoothbutton.model.DeviceLog;
import ru.nix.bluetoothbutton.model.RequestForSync;

@Dao
public abstract class ButtonPressStore {
    public static final int BUTTONS_NUMBER = 10;
    private static ExecutorService executor;


    public ButtonPressStore(){
        executor = Executors.newSingleThreadExecutor();
    }

    //Presses
    @Query("SELECT * FROM presses ORDER BY id")
    abstract public LiveData<List<ButtonPress>> selectAllLD();

    @Query("SELECT * FROM presses WHERE canceled = 0 ORDER BY date desc limit 100")
    abstract public LiveData<List<ButtonPress>> selectTopPressesLD();

    @Query("SELECT * FROM presses ORDER BY id")
    abstract public List<ButtonPress> selectAll();

    @Query("SELECT * FROM presses WHERE uploaded = 0")
    abstract public List<ButtonPress> selectUnUploaded();

    @Query("SELECT * FROM presses WHERE canceled = 0 ORDER BY date desc limit 1")
    abstract public List<ButtonPress> selectLastUnCanceled();

    @Insert
    abstract public void insert(ButtonPress... presses);

    @Update
    abstract public void update(ButtonPress... presses);

    @Delete
    abstract public void delete(ButtonPress... presses);

    //Statistics
    @Query("SELECT * FROM statistics ORDER BY buttonId")
    abstract public LiveData<List<ButtonStatistics>> selectAllStatisticsLD();

    @Query("SELECT * FROM statistics ORDER BY buttonId")
    abstract public List<ButtonStatistics> selectAllStatistics();

    @Query("SELECT * FROM statistics WHERE buttonId = :buttonId")
    abstract public List<ButtonStatistics> selectStatistics(int buttonId);

    @Insert
    abstract public void insert(ButtonStatistics... statistics);

    @Update
    abstract public void update(ButtonStatistics... statistics);

    @Delete
    abstract public void delete(ButtonStatistics... statistics);

    //Devices

    @Query("SELECT * FROM devices")
    abstract public LiveData<List<ButtonDevice>> selectAllDevices();

    @Query("SELECT * FROM devices WHERE address = :address")
    abstract public LiveData<List<ButtonDevice>> selectDeviceLD(String address);

    @Query("SELECT * FROM devices WHERE address = :address")
    abstract public List<ButtonDevice> selectDevice(String address);

    @Query("SELECT * FROM devices WHERE lastCanConnectReceived > 0 ORDER BY lastCanConnectReceived")
    abstract public LiveData<List<ButtonDevice>> selectCanConnectDevicesLD();

    @Query("SELECT * FROM devices WHERE lastCanConnectReceived > 1 ORDER BY lastCanConnectReceived")
    abstract public List<ButtonDevice> selectCanConnectDevices();

    @Query("SELECT * FROM devices WHERE connectionState = 2")
    abstract public List<ButtonDevice> selectConnectedDevices();

    @Query("SELECT * FROM devices WHERE lastConnected > 0 ORDER BY lastConnected desc")
    abstract public LiveData<List<ButtonDevice>> selectLastConnectedDevicesLD();

    @Query("SELECT * FROM devices WHERE lastConnected > 0 ORDER BY lastConnected desc")
    abstract public List<ButtonDevice> selectLastConnectedDevices();

    @Insert
    abstract public void insert(ButtonDevice... devices);

    @Update
    abstract public void update(ButtonDevice... devices);

    @Delete
    abstract public void delete(ButtonDevice... devices);

    //Button action
    @Query("SELECT * FROM action ORDER BY buttonId")
    abstract public LiveData<List<ButtonAction>> selectAllActionsLD();

    @Query("SELECT * FROM action ORDER BY buttonId")
    abstract public List<ButtonAction> selectAllActions();

    @Query("SELECT * FROM action WHERE buttonId = :buttonId")
    abstract public LiveData<List<ButtonAction>> selectActionLD(int buttonId);

    @Query("SELECT * FROM action WHERE buttonId = :buttonId")
    abstract public List<ButtonAction> selectAction(int buttonId);

    @Insert
    abstract public void insert(ButtonAction... actions);

    @Update
    abstract public void update(ButtonAction... actions);

    @Delete
    abstract public void delete(ButtonAction... actions);

    //Button action log -------------------------------------------------------------------------
    @Query("SELECT * FROM action_log WHERE buttonId = :buttonId")
    abstract public List<ButtonActionLog> selectActionsLogForButton(int buttonId);

    @Query("SELECT * FROM action_log WHERE buttonId = :buttonId AND dateTo IS NULL")
    abstract public List<ButtonActionLog> selectCurrentActionsLogItemForButton(int buttonId);

    @Query("SELECT * FROM action_log WHERE uploaded = 0 ")
    abstract public List<ButtonActionLog> selectUnuploadedActionLogs();

    @Insert
    abstract public void insert(ButtonActionLog... actions);

    @Update
    abstract public void update(ButtonActionLog... actions);

    @Delete
    abstract public void delete(ButtonActionLog... actions);

    // SyncRequest  ---------------------------------------------
    @Query("SELECT * FROM sync_request")
    abstract public LiveData<List<RequestForSync>> selectAllSyncRequestsLD();

    @Insert
    abstract public void insert(RequestForSync... requestsForSync);

    @Delete
    abstract public void delete(RequestForSync... requestsForSync);

    // DeviceLog -----------------------
    @Query("SELECT * FROM device_log WHERE uploaded = 0")
    abstract public List<DeviceLog> selectUnuploadedDeviceLogs();

    @Insert
    abstract public void insert(DeviceLog... deviceLogs);

    @Update
    abstract public void update(DeviceLog... deviceLogs);

    @Delete
    abstract public void delete(DeviceLog... deviceLogs);



    public void clearCanConnect(){
        executor.submit(()-> {
            List<ButtonDevice> canConnectDevices = selectCanConnectDevices();
            Log.d("happy2", "clearCanConnect");
            if (canConnectDevices != null && canConnectDevices.size() > 0) {
                Log.d("happy2", "clearCanConnect2");
                for (ButtonDevice buttonDevice : canConnectDevices) {
                    Log.d("happy2", "clearCanConnect3");
                    buttonDevice.lastCanConnectReceived = 0;
                    update(buttonDevice);
                }
            }
        });
    }


    public void addCanConnect(ButtonDevice buttonDevice) {
        executor.submit(() -> {
            List<ButtonDevice> storedButtonDevice = selectDevice(buttonDevice.address);
            Log.d("happy2", "addCanConnect " + buttonDevice.address);
            if (storedButtonDevice!= null && storedButtonDevice.size() > 0) {
                if (storedButtonDevice.get(0).lastCanConnectReceived == 0) {
                    storedButtonDevice.get(0).lastCanConnectReceived = System.currentTimeMillis();
                    update(storedButtonDevice.get(0));
                }
            } else {
                buttonDevice.lastCanConnectReceived = System.currentTimeMillis();
                buttonDevice.lastConnected = 0;
                buttonDevice.charge = 0;
                buttonDevice.connectionState = ConnectionState.DISCONNECTED;
                buttonDevice.lastChargeReceived = 0;
                insert(buttonDevice);
            }
        });


    }

    public void setConnectionState(String address, ConnectionState state){
        executor.submit(() -> {
            List<ButtonDevice> storedButtonDevice = selectDevice(address);

            if (storedButtonDevice!= null && storedButtonDevice.size() > 0) {
                storedButtonDevice.get(0).connectionState = state;
                if(state==ConnectionState.CONNECTING) {
                    storedButtonDevice.get(0).lastConnected = System.currentTimeMillis();
                }
                if(state==ConnectionState.CONNECTED) {
                    storedButtonDevice.get(0).lastConnected = System.currentTimeMillis();
                    storedButtonDevice.get(0).usbConnected = false;
                }
                update(storedButtonDevice.get(0));
            }
        });

    }

    public void setDisconnected(){
        executor.submit(() -> {
            List<ButtonDevice> storedButtonDevice = selectConnectedDevices();
            if (storedButtonDevice!= null && storedButtonDevice.size() > 0) {
                for(ButtonDevice buttonDevice: storedButtonDevice) {
                    buttonDevice.connectionState = ConnectionState.DISCONNECTED;
                    storedButtonDevice.get(0).usbConnected = false;
                    update(buttonDevice);
                }
            }
        });

    }

    public void updateBatteryLevel(float batteryLevel){
        executor.submit(() -> {
            List<ButtonDevice> storedButtonDevice = selectConnectedDevices();
            if (storedButtonDevice!= null && storedButtonDevice.size() > 0) {
                storedButtonDevice.get(0).charge = batteryLevel;
                storedButtonDevice.get(0).lastChargeReceived = System.currentTimeMillis();
                update(storedButtonDevice.get(0));
            }
        });


    }

    public void updateUsbConnected(boolean usbConnected){
        executor.submit(() -> {
            List<ButtonDevice> storedButtonDevice = selectConnectedDevices();
            if (storedButtonDevice!= null && storedButtonDevice.size() > 0) {
                storedButtonDevice.get(0).usbConnected = usbConnected;
                update(storedButtonDevice.get(0));
            }
        });


    }

    public void incrementButtonStatistics(int buttonId){
        List<ButtonStatistics> statisticsList = selectStatistics(buttonId);
        if(statisticsList!=null && statisticsList.size()>0){
            ButtonStatistics statistics = statisticsList.get(0);
            statistics.pressesNumber++;
            update(statistics);
        }else{
            insert(new ButtonStatistics(buttonId,1));
        }
    }

    public void decrementButtonStatistics(int buttonId){
        List<ButtonStatistics> statisticsList = selectStatistics(buttonId);
        if(statisticsList!=null && statisticsList.size()>0){
            ButtonStatistics statistics = statisticsList.get(0);
            statistics.pressesNumber--;
            update(statistics);
        }else{
            insert(new ButtonStatistics(buttonId,1));
        }
    }

    public void resetButtonStatistics(){
        executor.submit(() -> {
            List<ButtonStatistics> statisticsList = selectAllStatistics();
            if(statisticsList!=null){
                for(ButtonStatistics statistics: statisticsList) {
                    statistics.pressesNumber=0;
                    update(statistics);
                }
            }
        });
    }

    public void initButtonStatistics(){
        executor.submit(() -> {
        for(int buttonId=1;buttonId<=BUTTONS_NUMBER;buttonId++){
            List<ButtonStatistics> statisticsList = selectStatistics(buttonId);
            if(statisticsList==null || statisticsList.size()==0){
                insert(new ButtonStatistics(buttonId,0));
            }
        }
        });
    }

    public void initButtonActions(){
        executor.submit(() -> {
            for(int buttonId=1;buttonId<=BUTTONS_NUMBER;buttonId++){
                List<ButtonAction> actionsList = selectAction(buttonId);
                if(actionsList==null || actionsList.size()==0){
                    insert(new ButtonAction(buttonId, null, false));
                }
            }
        });
    }

    public void initButtonActionsLog(){
        executor.submit(() -> {
            String initId = UUID.randomUUID().toString();
            for(int buttonId=1;buttonId<=BUTTONS_NUMBER;buttonId++){
                List<ButtonActionLog> actionsLogList = selectActionsLogForButton(buttonId);
                if(actionsLogList==null || actionsLogList.size()==0){
                    insert(new ButtonActionLog(buttonId,null, false,initId));
                }
            }
            requestSyncActions();
        });
    }



    public void addButtonPress(ButtonPress buttonPress){
        executor.submit(() -> {
          ButtonAction buttonAction = null;
          List<ButtonAction> actions = selectAction(buttonPress.buttonId);
          if(actions != null && actions.size() > 0){
              buttonAction = actions.get(0);
          }
          if(buttonAction!=null && buttonAction.cancelPrevious){
              Integer buttonId = cancelLastUnCanceledButtonPress();
              if(buttonId!=null) {
                  decrementButtonStatistics(buttonId);
              }
          }else {
              buttonPress.action = buttonAction.action;
              insert(buttonPress);
              requestSyncPresses();
              incrementButtonStatistics(buttonPress.buttonId);
          }
        });
    }

    private Integer cancelLastUnCanceledButtonPress(){
        List<ButtonPress> lastUnCanceledDataPress = selectLastUnCanceled();
        if(lastUnCanceledDataPress != null && lastUnCanceledDataPress.size()>0){
            ButtonPress press = lastUnCanceledDataPress.get(0);
            press.canceled = true;
            press.uploaded = false;
            update(press);
            requestSyncPresses();
            return press.buttonId;
        }

        return null;
    }

    public void updateButtonAction(ButtonAction buttonAction){
        executor.submit(() -> {
            update(buttonAction);
            List<ButtonActionLog> curLog = selectCurrentActionsLogItemForButton(buttonAction.buttonId);
            if(curLog!=null && curLog.size()>0){
                Date date = curLog.get(0).updateDateTo();
                update(curLog.get(0));
                insert(new ButtonActionLog(buttonAction.buttonId,buttonAction.action, buttonAction.cancelPrevious, date, curLog.get(0).initId));
                requestSyncActions();
            }
        });
    }

    private void requestSyncPresses(){
        requestSync(RequestForSync.REQUEST_SYNC_PRESSES);
    }

    private void requestSyncActions(){
        requestSync(RequestForSync.REQUEST_SYNC_ACTIONS);
    }

    private void requestSync (String syncType){
        executor.submit(() -> {
            insert(new RequestForSync(syncType));
        });
    }

    public void removeSyncRequests(List<RequestForSync> requests){
        executor.submit(() -> {
            for(RequestForSync request: requests) delete(request);
        });
    }

    public void addDeviceLog(DeviceLog deviceLog){
        executor.submit(() -> {
            insert(deviceLog);
        });
    }

    public void setUploadedButtonPresses(List<ButtonPress> buttonPresses){
        executor.submit(() -> {
            for(ButtonPress buttonPress: buttonPresses) {
                buttonPress.uploaded=true;
                update(buttonPress);
            };
        });
    }
    public void setUploadedButtonActionLogs(List<ButtonActionLog> buttonActionLogs){
        executor.submit(() -> {
            for(ButtonActionLog buttonActionLog: buttonActionLogs) {
                buttonActionLog.uploaded=true;
                update(buttonActionLog);
            };
        });
    }

    public void setUploadedDeviceLogs(List<DeviceLog> deviceLogs){
        executor.submit(() -> {
            for(DeviceLog deviceLog: deviceLogs) {
                deviceLog.uploaded=true;
                update(deviceLog);
            };
        });
    }



}
