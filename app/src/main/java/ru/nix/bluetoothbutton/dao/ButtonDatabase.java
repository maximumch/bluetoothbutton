package ru.nix.bluetoothbutton.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.support.annotation.NonNull;

import ru.nix.bluetoothbutton.model.ButtonAction;
import ru.nix.bluetoothbutton.model.ButtonActionLog;
import ru.nix.bluetoothbutton.model.ButtonDevice;
import ru.nix.bluetoothbutton.model.ButtonPress;
import ru.nix.bluetoothbutton.model.ButtonStatistics;
import ru.nix.bluetoothbutton.model.DeviceLog;
import ru.nix.bluetoothbutton.model.RequestForSync;


@Database(entities={ButtonPress.class, ButtonStatistics.class, ButtonDevice.class, ButtonAction.class, ButtonActionLog.class, RequestForSync.class, DeviceLog.class},
        version=1)
@TypeConverters({TypeTransmogrifier.class})
public abstract class ButtonDatabase extends RoomDatabase  {
    public abstract ButtonPressStore buttonPressStore();
    //private static ExecutorService executor;

    private static final String DB_NAME = "buttonsRoom.db";
    private static volatile ButtonDatabase INSTANCE=null;

    public synchronized static ButtonDatabase get(Context ctxt) {
        if (INSTANCE == null) {
            INSTANCE = create(ctxt, false);
        }
        return (INSTANCE);
    }

    public static ButtonDatabase create(Context ctxt, boolean memoryOnly) {
        RoomDatabase.Builder<ButtonDatabase> b;
        //executor = Executors.newSingleThreadExecutor();
        if (memoryOnly) { b= Room.inMemoryDatabaseBuilder(ctxt.getApplicationContext(),
                ButtonDatabase.class);
        }
        else {
            b=Room.databaseBuilder(ctxt.getApplicationContext(), ButtonDatabase.class,
                    DB_NAME);



        }
        return(b.build());
    }

    @Override
    public void init(@NonNull DatabaseConfiguration configuration) {
        super.init(configuration);
        ButtonPressStore store = buttonPressStore();
        store.initButtonStatistics();
        store.initButtonActions();
        store.initButtonActionsLog();
    }
}
