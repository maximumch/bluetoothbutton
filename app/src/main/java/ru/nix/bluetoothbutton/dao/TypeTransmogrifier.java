package ru.nix.bluetoothbutton.dao;

import android.arch.persistence.room.TypeConverter;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TypeTransmogrifier {


    private static SimpleDateFormat format;

    static{
        format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    }


    @TypeConverter
    public static synchronized String fromDate(Date date) {
        if (date==null) {
            return(null);
        }
        //Log.v("happyTranform", "from date: " + date );
        return format.format(date);

    }

    @TypeConverter
    public static synchronized Date toDate(String str) throws RuntimeException {
        if (str==null) {
            return(null);
        }
        try {
            //Log.v("happyTranform", "to date: " + str );
            return(format.parse(str));

        } catch (ParseException e) {
            throw new RuntimeException("Can not convert String to Date", e);
        }
    }



}
