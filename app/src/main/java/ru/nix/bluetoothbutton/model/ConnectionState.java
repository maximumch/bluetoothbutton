package ru.nix.bluetoothbutton.model;

import android.arch.persistence.room.TypeConverter;

public enum ConnectionState {
    DISCONNECTED(0), CONNECTING(1), CONNECTED(2);

    private final int state;

    @TypeConverter
    public static ConnectionState fromState(Integer state) {
        for (ConnectionState p : values()) {
            if (p.state == state) {
                return (p);
            }
        }
            return (null);

    }
    @TypeConverter
    public static Integer fromConnectionState(ConnectionState p) {
        return(p.state);
    }

    ConnectionState(int state){
        this.state = state;
    }



}
