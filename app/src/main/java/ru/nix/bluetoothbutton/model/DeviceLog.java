package ru.nix.bluetoothbutton.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;


import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

@Entity(tableName = "device_log")
public class DeviceLog {
    @PrimaryKey
    @NonNull
    @Expose
    public String id;
    @Expose
    public Date date;
    @Expose
    public String message;
    public boolean uploaded;

    @Ignore
    public DeviceLog(String message) {
        this.id = UUID.randomUUID().toString();
        date = (new GregorianCalendar()).getTime();
        this.message = message;
        this.uploaded = false;
    }

    public DeviceLog(@NonNull String id, Date date, String message, boolean uploaded) {
        this.id = id;
        this.date = date;
        this.message = message;
        this.uploaded = uploaded;
    }
}
