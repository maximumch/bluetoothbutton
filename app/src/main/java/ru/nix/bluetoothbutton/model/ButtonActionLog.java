package ru.nix.bluetoothbutton.model;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

@Entity(tableName = "action_log")

public class ButtonActionLog {
    @PrimaryKey
    @NonNull
    @Expose
    public String id;
    @Expose
    public int buttonId;
    @Expose
    public String action;
    @Expose
    public boolean cancelPrevious;
    @Expose
    public Date dateFrom;
    @Expose
    public Date dateTo;
    public boolean uploaded;
    @NonNull
    @Expose
    public String initId;


    @Ignore
    public ButtonActionLog(int buttonId, String action, boolean cancelPrevious, String initId) {
        this.buttonId = buttonId;
        id = UUID.randomUUID().toString();
        this.action = action;
        this.cancelPrevious = cancelPrevious;
        dateFrom = (new GregorianCalendar()).getTime();
        dateTo = null;
        uploaded = false;
        this.initId = initId;

    }

    @Ignore
    public ButtonActionLog(int buttonId, String action, boolean cancelPrevious, Date dateFrom, String initId) {
        this.buttonId = buttonId;
        id = UUID.randomUUID().toString();
        this.action = action;
        this.cancelPrevious = cancelPrevious;
        this.dateFrom = dateFrom;
        dateTo = null;
        uploaded = false;
        this.initId = initId;

    }

    @Ignore
    public Date updateDateTo(){
        dateTo = (new GregorianCalendar()).getTime();
        uploaded = false;
        return dateTo;
    }

    public ButtonActionLog(@NonNull String id, int buttonId, String action, boolean cancelPrevious, Date dateFrom, Date dateTo, boolean uploaded, @NonNull String initId) {
        this.id = id;
        this.buttonId = buttonId;
        this.action = action;
        this.cancelPrevious = cancelPrevious;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.uploaded = uploaded;
        this.initId = initId;
    }
}
