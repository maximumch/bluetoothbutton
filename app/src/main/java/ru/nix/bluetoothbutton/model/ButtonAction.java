package ru.nix.bluetoothbutton.model;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
@Entity(tableName = "action")
public class ButtonAction {
    @PrimaryKey
    @NonNull
    public int buttonId;
    public String action;
    public boolean cancelPrevious;


    public ButtonAction(@NonNull int buttonId, String action, boolean cancelPrevious) {
        this.buttonId = buttonId;
        this.action = action;
        this.cancelPrevious = cancelPrevious;
    }

    @Override
    public String toString() {
        return ("button " + buttonId + " - " + action);

    }

    public boolean equals(ButtonAction b){
        return (buttonId==b.buttonId &&
                ((action==null && b.action==null) || action.equals(b.action)) &&
                cancelPrevious==b.cancelPrevious
                );
    }





}
