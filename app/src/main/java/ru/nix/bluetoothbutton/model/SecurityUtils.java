package ru.nix.bluetoothbutton.model;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SecurityUtils {
    public static String getPackageSHA1(Context context) {
        // Определение SHA1
        String something = "";
        PackageInfo info;
        try {


            info = context.getPackageManager().getPackageInfo(
                    context.getPackageName()
                    , PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());


                something = new String(Base64.encode(md.digest(), Base64.NO_WRAP));

            }

        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }

        return something;
    }
}
