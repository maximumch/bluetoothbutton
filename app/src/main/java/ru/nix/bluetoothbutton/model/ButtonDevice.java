package ru.nix.bluetoothbutton.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;



@Entity(tableName = "devices")
public class ButtonDevice {
    @PrimaryKey
    @NonNull
    public String address;
    public String name;
    public double charge;
    public long lastChargeReceived;
    public long lastCanConnectReceived;
    @TypeConverters({ConnectionState.class})
    public ConnectionState connectionState;
    public long lastConnected;
    public long lastInfoUpdated;
    public boolean usbConnected;


    public ButtonDevice(@NonNull String address, String name, double charge, long lastChargeReceived, long lastCanConnectReceived
            , ConnectionState connectionState, long lastConnected, long lastInfoUpdated, boolean usbConnected) {
        this.address = address;
        this.name = name;
        this.charge = charge;
        this.lastChargeReceived = lastChargeReceived;
        this.lastCanConnectReceived = lastCanConnectReceived;
        this.connectionState = connectionState;
        this.lastConnected = lastConnected;
        this.lastInfoUpdated = lastInfoUpdated;
        this.usbConnected = usbConnected;
    }

    @Ignore
    public ButtonDevice(BluetoothDevice device) {
        this.address = device.getAddress();
        this.name = device.getName();
    }



    @Override
    public String toString() {
        return(address+ " - " + String.valueOf(lastConnected));
    }



}
