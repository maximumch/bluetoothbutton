package ru.nix.bluetoothbutton.model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.UUID;

@Entity(tableName = "sync_request")
public class RequestForSync {
    @Ignore
    public static final String REQUEST_SYNC_PRESSES = "presses";
    @Ignore
    public static final String REQUEST_SYNC_ACTIONS = "actions";

    @PrimaryKey
    @NonNull
    public String id;

    @NonNull
    public String request;

    public RequestForSync(@NonNull String request) {
        this.id = UUID.randomUUID().toString();
        this.request = request;
    }
}
