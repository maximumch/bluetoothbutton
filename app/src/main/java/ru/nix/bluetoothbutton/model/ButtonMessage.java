package ru.nix.bluetoothbutton.model;

import android.content.Intent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Chernyshev on 11.05.18.
 */

public class ButtonMessage {
    private MessageType messageType;
    private Date date;
    private int buttonNumber=0;
    private float voltage=0;
    private float percentage=0;




    public enum MessageType{BUTTON_DOWN, BUTTON_UP, BATTERY_LEVEL, INACTIVE_TIMEOUT, BLUETOOTH_OFF, BATTERY_LOW, POWER_OFF, USB_CONNECTED, USB_DISCONNECTED, CHARGING_STARTED, CHARGING_FINISHED, INACTIVE}


    public ButtonMessage(Date date, int buttonNumber, MessageType messageType) {
        this.date = date;
        this.buttonNumber = buttonNumber;
        this.messageType = messageType;
    }

    public ButtonMessage(MessageType messageType, Date date) {
        this.messageType = messageType;
        this.date = date;

    }

    public Date getDate() {
        return date;
    }

    public int getButtonNumber() {
        return buttonNumber;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public float getVoltage() {
        return voltage;
    }

    public float getPercentage() {
        return percentage;
    }

    private static class MessageTypeParams{
        private MessageType messageType;
        private String pattern;
        private int[]  buttonNumberGroups;
        private int[]  voltageGroups;
        private int[]  percentageGroups;

        public MessageTypeParams(MessageType messageType, String pattern, int[] buttonNumberGroups, int[] voltageGroups, int[] percentageGroups) {
            this.messageType = messageType;
            this.pattern = pattern;
            this.buttonNumberGroups = buttonNumberGroups;
            this.voltageGroups = voltageGroups;
            this.percentageGroups = percentageGroups;
        }

        public MessageType getMessageType() {
            return messageType;
        }

        public String getPattern() {
            return pattern;
        }

        public int[] getButtonNumberGroups() {
            return buttonNumberGroups;
        }

        public int[] getVoltageGroups() {
            return voltageGroups;
        }

        public int[] getPercentageGroups() {
            return percentageGroups;
        }
    }

    public static class MessageParser {
        private static final MessageTypeParams[] messageTypeParamsArray;
        private String buff="";
        private List<ButtonMessage> newMessages = new ArrayList<>();

        static {
            messageTypeParamsArray = new MessageTypeParams[]{
                new MessageTypeParams(MessageType.BUTTON_DOWN, "([$]BUTTON_DOWN,)(\\d{1})", new int[]{2}, null, null),
                        new MessageTypeParams(MessageType.BUTTON_UP, "([$]BUTTON_UP,)(\\d{1})", new int[]{2}, null, null),
                        new MessageTypeParams(MessageType.BATTERY_LEVEL, "([$]BAT,)(\\d{1})(.)(\\d{3})(V,)(\\d{3})(.)(\\d{2})(%)", null, new int[]{2, 3, 4}, new int[]{6, 7, 8}),
                        new MessageTypeParams(MessageType.INACTIVE_TIMEOUT, "([$]INACTIVE_TIMEOUT)", null, null, null),
                        new MessageTypeParams(MessageType.BLUETOOTH_OFF, "([$]BLUETOOTH_OFF)", null, null, null),
                        new MessageTypeParams(MessageType.BATTERY_LOW, "([$]BATTERY_LOW)", null, null, null),
                        new MessageTypeParams(MessageType.POWER_OFF, "([$]POWER_OFF)", null, null, null),
                        new MessageTypeParams(MessageType.USB_CONNECTED, "([$]USB_CONNECTED)", null, null, null),
                        new MessageTypeParams(MessageType.USB_DISCONNECTED, "([$]USB_DISCONNECTED)", null, null, null),
                        new MessageTypeParams(MessageType.CHARGING_STARTED, "([$]CHARGING_STARTED)", null, null, null),
                        new MessageTypeParams(MessageType.CHARGING_FINISHED, "([$]CHARGING_FINISHED)", null, null, null),
                        new MessageTypeParams(MessageType.INACTIVE, "([$]INACTIVE,)(\\d{5})", null, null, new int[]{2})
            } ;
        }

        public synchronized void writeBytes(String str){
            if(str==null) return;
            buff += str;
            parseMessages();
        }

        public synchronized List<ButtonMessage> getNewMessages() {
            List<ButtonMessage> result = newMessages;
            newMessages = new ArrayList<>();
            return result;
        }

        private void parseMessages(){
            Pattern pattern;
            Matcher matcher;
            int position;

            ButtonMessage message;
            ArrayList<Integer> positions = new ArrayList<>();

            for (MessageTypeParams params:messageTypeParamsArray) {
                pattern = Pattern.compile(params.pattern);
                matcher = pattern.matcher(buff);
                while(matcher.find()){
                    message = new ButtonMessage(params.messageType, GregorianCalendar.getInstance().getTime());
                    if(params.getButtonNumberGroups()!=null){
                        int buttonNumber = Integer.decode(getGroupsText(matcher,params.getButtonNumberGroups()));
                        message.buttonNumber = buttonNumber;
                    }

                    if(params.getVoltageGroups()!=null){
                        float voltage = Float.parseFloat(getGroupsText(matcher,params.getVoltageGroups()));
                        message.voltage = voltage;
                    }

                    if(params.getPercentageGroups()!=null){
                        float percentage = Float.parseFloat(getGroupsText(matcher,params.getPercentageGroups()));
                        message.percentage = percentage;
                    }

                    newMessages.add(message);
                    position = matcher.end(matcher.groupCount());
                    positions.add(position);
                }
            }

            if(positions.size()>0) {
                int maxPosition = Collections.max(positions);
                buff = buff.substring(maxPosition);
            }
        }

        private String getGroupsText(Matcher matcher, int[] groups){
            String text="";
            for(int group: groups){
                text += matcher.group(group);
            }
            return text;
        }


    }



}
