package ru.nix.bluetoothbutton.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

@Entity(tableName = "presses")
public class ButtonPress {
    @PrimaryKey
    @NonNull
    @Expose
    public String id;
    @Expose
    public Date date;
    @Expose
    public int buttonId;
    public boolean uploaded;
    @Expose
    public String action;
    @Expose
    public boolean canceled;



    @Ignore
    public ButtonPress(int buttonId) {
        this.buttonId = buttonId;
        id = UUID.randomUUID().toString();
        date = (new GregorianCalendar()).getTime();
        uploaded = false;
        action = null;
        canceled = false;

    }

    public ButtonPress(@NonNull String id, Date date, int buttonId, boolean uploaded, String action, boolean canceled) {
        this.id = id;
        this.date = date;
        this.buttonId = buttonId;
        this.uploaded = uploaded;
        this.action = action;
        this.canceled = canceled;
    }

    @Override
    public String toString() {
        return("id=" + id + " date=" + date + "buttonId=" + buttonId);
    }


}
