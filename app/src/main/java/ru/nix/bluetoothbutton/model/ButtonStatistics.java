package ru.nix.bluetoothbutton.model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
@Entity(tableName = "statistics")
public class ButtonStatistics {
    @PrimaryKey
    @NonNull
    public int buttonId;
    public int pressesNumber;


    public ButtonStatistics(@NonNull int buttonId, int pressesNumber) {
        this.buttonId = buttonId;
        this.pressesNumber = pressesNumber;
    }

    @Override
    public String toString() {
        return ("button " + buttonId + " - " + pressesNumber + " presses");

    }
}
