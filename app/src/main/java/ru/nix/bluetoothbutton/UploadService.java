package ru.nix.bluetoothbutton;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.nix.bluetoothbutton.dao.ButtonDatabase;
import ru.nix.bluetoothbutton.dao.ButtonPressStore;
import ru.nix.bluetoothbutton.model.ButtonPress;
import ru.nix.bluetoothbutton.model.SecurityUtils;
import ru.nix.bluetoothbutton.network.ButtonWebService;



public class UploadService extends IntentService {
    public static final String BASE_URL = "http://server-vtayl.nix.ru";
    public static final String KEY_SIGNATURE = "ru.maxch.customauthentificator.signature";
    public static final String ACCOUNT_TYPE = "ru.nix.nixpassport";
    private Retrofit retrofit;
    private ButtonWebService service;
    private ButtonPressStore store;
    private ButtonDatabase db;

    public enum WebRequestResult {OK, ACCESS_DENIED, NETWORK_ERROR}

    public UploadService() {
               super(".UploadService");


         Gson gson = new GsonBuilder()
         .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();


        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        service = retrofit.create(ButtonWebService.class);
        db = ButtonDatabase.get(this);
        store=db.buttonPressStore();
    }




    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        List<String> tokens = getTokens();
        List<String> invalidatedTokens = new ArrayList<>();
        for(String token: tokens){
            if(uploadPresses(token)==WebRequestResult.ACCESS_DENIED){
                invalidatedTokens.add(token);
            }
        }

        if(invalidatedTokens.size()>0){
            invalidateTokens(invalidatedTokens);
            tokens = getTokens();
            for(String token: tokens){
                uploadPresses(token);
            }
        }






//        List<String> tokens = getTokens();
//        if(tokens.size()>0){
//            Log.v("happy", "token = " + tokens.get(0));
//        }
    }

    private void invalidateTokens(List<String> invalidatedTokens) {
        AccountManager accountManager = AccountManager.get(this);
        for(String token: invalidatedTokens) {
            accountManager.invalidateAuthToken(ACCOUNT_TYPE, token);
        }

    }

    private WebRequestResult uploadPresses(String token) {
        List<ButtonPress> buttonPressList = store.selectAll();
        Call<Void> call = service.addButtonPressList(token,buttonPressList);

        try {
            Response<Void> response = call.execute();
            if(response.isSuccessful()){
                for(ButtonPress buttonPress: buttonPressList) {
                    store.delete(buttonPress);
                }
                return WebRequestResult.OK;
            }else{
                if(response.code()==403){
                    return WebRequestResult.ACCESS_DENIED;
                }else{
                    return WebRequestResult.NETWORK_ERROR;
                }
            }
        } catch (IOException e) {
            return WebRequestResult.NETWORK_ERROR;
        }
    }

    private List<String> getTokens(){
        List<AccountManagerFuture<Bundle>> blist = new ArrayList<>();
        List<String> tokens = new ArrayList<>();


        AccountManager accountManager = AccountManager.get(this);
        Account[] accounts;
        accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
        if(accounts!=null){
            StringBuilder stringBuilder = new StringBuilder();

            for(Account account:accounts){
                AccountManagerFuture<Bundle> b;
                Bundle options = new Bundle();
                options.putString(KEY_SIGNATURE, SecurityUtils.getPackageSHA1(this));
                b=accountManager.getAuthToken(account,this.getPackageName(),options,false,null,null);

                blist.add(b);
            }

            for(AccountManagerFuture<Bundle> b:blist){
                try {
                    Bundle bundle = b.getResult();
                    String token;
                    token = bundle.getString(AccountManager.KEY_AUTHTOKEN,null);
                    if(token != null){
                        tokens.add(token);
                    }

                } catch (OperationCanceledException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (AuthenticatorException e) {
                    e.printStackTrace();
                }
            }

        }

        return tokens;




    }


}
