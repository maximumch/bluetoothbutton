package ru.nix.bluetoothbutton;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import ru.nix.bluetoothbutton.hardware.HardwareAdapter;

public class BlutoothButtonApplication extends Application  {
    private  HardwareAdapter hardwareAdapter;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("happy", "BlutoothButtonApplication onCreate" );
        Intent intent = new Intent(this, BluetoothService.class);
        intent.putExtra(BluetoothService.EXTRA_ACTION, BluetoothService.ACTION_INITIAL_START);
        startService(intent);


    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
