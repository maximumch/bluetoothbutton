package ru.nix.bluetoothbutton.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Chernyshev on 15.06.18.
 */

public class LogHelper extends SQLiteOpenHelper {
    public static final String LOG_DATABASE = "log.db";
    public static final int LOG_DATABASE_VERSION = 3;

    public LogHelper(Context context) {
        super(context, LOG_DATABASE, null, LOG_DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        LogTable.onCreate(sqLiteDatabase);
        ButtonsTable.onCreate(sqLiteDatabase);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        LogTable.onUpgrade(sqLiteDatabase, i, i1);
        ButtonsTable.onUpgrade(sqLiteDatabase, i, i1);
    }
}
