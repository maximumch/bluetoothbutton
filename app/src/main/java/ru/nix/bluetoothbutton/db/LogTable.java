package ru.nix.bluetoothbutton.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

/**
 * Created by Chernyshev on 15.06.18.
 */

public class LogTable {
    public static final String TABLE_LOG = "log";
    public static final String COLUMN_LOG_ID = "_id";
    public static final String COLUMN_LOG_DATE = "date";
    public static final String COlUMN_LOG_ACTION = "action";


    private static final String LOG_CREATE = "create table  "
            + TABLE_LOG + "("
            + COLUMN_LOG_ID
            + "  integer primary key autoincrement, "
            + COLUMN_LOG_DATE
            + "  text not null, "
            + COlUMN_LOG_ACTION
            + "   text not null "
            + ");";

    public static final String LOG_SELECT = "select top 100 " + COLUMN_LOG_DATE +",   " +COlUMN_LOG_ACTION +"  from ";
    public static final String LOG_INSERT = "insert into log(date, action) values (?,?)";


    public static void onCreate(SQLiteDatabase sqLiteDatabase){
        sqLiteDatabase.execSQL(LOG_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1){
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_LOG);
        onCreate(sqLiteDatabase);
    }


}
