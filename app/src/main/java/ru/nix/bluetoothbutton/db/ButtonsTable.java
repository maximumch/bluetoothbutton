package ru.nix.bluetoothbutton.db;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Chernyshev on 22.06.18.
 */

public class ButtonsTable {
    public static final String TABLE_BUTTONS = "buttons";
    public static final String COLUMN_BUTTONS_ID = "_id";
    public static final String COLUMN_BUTTONS_NUMBER = "number";
    public static final String COlUMN_BUTTONS_PRESSES_NUMBER = "presses_number";


    private static final String LOG_CREATE = "create table  "
            + TABLE_BUTTONS + "("
            + COLUMN_BUTTONS_ID
            + "  integer primary key autoincrement, "
            + COLUMN_BUTTONS_NUMBER
            + "  integer not null, "
            + COlUMN_BUTTONS_PRESSES_NUMBER
            + "   integer not null "
            + ");";

    //public static final String LOG_SELECT = "select top 100 " + COLUMN_LOG_DATE +",   " +COlUMN_LOG_ACTION +"  from ";
    public static final String BUTTONS_INSERT = "insert into buttons(number, presses_number) values (?,?)";

    public static void onCreate(SQLiteDatabase sqLiteDatabase){
        sqLiteDatabase.execSQL(LOG_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1){
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_BUTTONS);
        onCreate(sqLiteDatabase);
    }
}
