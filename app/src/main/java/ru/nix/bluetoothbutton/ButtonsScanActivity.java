package ru.nix.bluetoothbutton;

import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.widget.ImageViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.nix.bluetoothbutton.dao.ButtonDatabase;
import ru.nix.bluetoothbutton.model.ButtonDevice;

public class ButtonsScanActivity extends AppCompatActivity implements  SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout swipeContainer;

    private RecyclerView recyclerView;
    private DeviceAdapter adapter;
    private State state;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buttons_scan);

        swipeContainer =  findViewById(R.id.swipe_refresh_layout);
        swipeContainer.setOnRefreshListener(this);

        recyclerView = findViewById(R.id.recycler);
        state=(State)getLastCustomNonConfigurationInstance();
        if (state==null) {
            state=new State();
            state.eventMutableLiveData= BluetoothService.BUSDevices;
        }

        swipeContainer.setRefreshing(state.scanning);

        adapter = new DeviceAdapter();

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

        state.eventMutableLiveData.observe( this, event -> onChanged(event));
        ButtonDatabase.get(this).buttonPressStore().selectCanConnectDevicesLD().observe(this, buttonDevices -> onChanged(buttonDevices));



    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return(state);
    }


    private void onChanged(@Nullable BluetoothService.Event event) {
        Log.v("happy", "onChanged event" + event.getType().toString());
        if(!state.events.contains(event)) {
            state.events.add(event);

            switch (event.getType()) {
                case CONNECTED:
                    break;
                case DISCONNECTED:

                    break;
                case SCAN_STARTED:
                    state.scanning = true;
                    swipeContainer.setRefreshing(true);
                    break;
                case SCAN_FINISHED:
                    state.scanning = false;
                    swipeContainer.setRefreshing(false);
                    break;
                case CONNECTING_FAILED:
                    break;
                case DEVICE_DISCOVERED:

//                    ButtonDevice buttonDevice = new ButtonDevice(event.getDevice().);
//                    buttonDevice.device = event.getDevice().device;
//                    buttonDevice.previouslyConnected = true;
//                    state.devices.add(buttonDevice);
//                    adapter.notifyItemInserted(state.devices.size() - 1);
                    break;
                case CONNECTING_STARTED:
                    break;
            }
        }
    }


    @Override
    public void onRefresh() {
        Intent intent = new Intent(this, BluetoothService.class);
        intent.putExtra(BluetoothService.EXTRA_ACTION,BluetoothService.ACTION_START_SCAN);
        startService(intent);
    }


    public void onChanged(@Nullable List<ButtonDevice> buttonDevices) {
        Log.d("happy2", "onChanged ");
        int n = adapter.devices.size();
        if(n>0){
            Log.d("happy2", "clear devices n = " + n);
            adapter.devices.clear();
            adapter.notifyItemRangeRemoved(0,n);
        }
        if(buttonDevices!= null && buttonDevices.size()>0) {
            Log.d("happy2", "add devices n = " + buttonDevices.size());
            adapter.devices.addAll(buttonDevices);
            adapter.notifyItemRangeInserted(0, buttonDevices.size() - 1);
        }
    }

    private static class State {

        final ArrayList<BluetoothService.Event> events=new ArrayList<>();
        MutableLiveData<BluetoothService.Event> eventMutableLiveData;
        boolean scanning=false;
    }

    private class DeviceAdapter extends RecyclerView.Adapter<DeviceHolder> {
        final ArrayList<ButtonDevice> devices=new ArrayList<>();
        @Override
        public DeviceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View deviceView=
                    getLayoutInflater()
                            .inflate(R.layout.button_item2, parent, false);


            return(new DeviceHolder(deviceView, device -> connect(device)));
        }

        @Override
        public void onBindViewHolder(DeviceHolder holder, int position) {
            holder.bind(devices.get(position));
        }

        @Override
        public int getItemCount() {
            return devices.size();
        }
    }



    private static class DeviceHolder extends RecyclerView.ViewHolder {

        private final TextView address;
        private final TextView name;
        private ButtonDevice buttonDevice;
        private OnDeviceClickListener onClickListener;
        private ImageView lastConnected;
        //private final ImageView lastConnected;

        DeviceHolder(View itemView, OnDeviceClickListener listener) {
            super(itemView);
            address = itemView.findViewById(R.id.address);
            name = itemView.findViewById(R.id.name);
            lastConnected = itemView.findViewById(R.id.last_connected);
            this.onClickListener = listener;
            itemView.setOnClickListener(view -> this.onClickListener.onClick(this.buttonDevice));
        }

        void bind(ButtonDevice device) {
            address.setText(device.address);
            name.setText(device.name);
            buttonDevice = device;
            if(device.lastConnected>0){
                lastConnected.setImageResource(R.drawable.ic_bluetooth_connected_green_24dp);
            }else{
                lastConnected.setImageResource(R.drawable.ic_bluetooth_connected_light_24dp);
            }


        }



    }

    interface OnDeviceClickListener{
        void onClick(ButtonDevice device);
    }

    private void connect(ButtonDevice buttonDevice){
        Intent connectIntent = new Intent(this,BluetoothService.class);
        connectIntent.putExtra(BluetoothService.EXTRA_ACTION,BluetoothService.ACTION_CONNECT);
        connectIntent.putExtra(BluetoothService.EXTRA_DEVICE, buttonDevice.address);
        startService(connectIntent);
        this.finish();
    }
}
