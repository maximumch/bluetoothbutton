package ru.nix.bluetoothbutton.network;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.nix.bluetoothbutton.model.ButtonAction;
import ru.nix.bluetoothbutton.model.ButtonActionLog;
import ru.nix.bluetoothbutton.model.ButtonPress;
import ru.nix.bluetoothbutton.model.DeviceLog;

public interface ButtonWebService {
    @POST("/BlueToothButton.ashx/button_presses")
    Call<Void> addButtonPressList (
            @Query("key") String apiKey,
            @Body List<ButtonPress> buttonPresses
            );

    @POST("/BlueToothButton.ashx/action_log")
    Call<Void> addButtonActionLogList (
            @Query("key") String apiKey,
            @Body List<ButtonActionLog> buttonActionLogs
    );

    @POST("/BlueToothButton.ashx/device_log")
    Call<Void> addDeviceLogList (
            @Query("key") String apiKey,
            @Body List<DeviceLog> deviceLogs
    );




}
