package ru.nix.bluetoothbutton;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.nix.bluetoothbutton.dao.ButtonDatabase;
import ru.nix.bluetoothbutton.dao.ButtonPressStore;
import ru.nix.bluetoothbutton.model.ButtonActionLog;
import ru.nix.bluetoothbutton.model.ButtonPress;
import ru.nix.bluetoothbutton.model.DeviceLog;
import ru.nix.bluetoothbutton.model.SecurityUtils;
import ru.nix.bluetoothbutton.network.ButtonWebService;
import ru.nix.bluetoothbutton.network.SslUtils;

public class SyncAdapter extends AbstractThreadedSyncAdapter {
    public static final String BASE_URL = "https://server-vtayl.nix.ru";
    public static final String KEY_SIGNATURE = "ru.nix.nixpassport.signature";
    public static final String ACCOUNT_TYPE = "ru.nix.nixpassport";
    private ButtonWebService service;
    private ButtonPressStore store;
    private AccountManager accountManager;
    private String packageSHA1;
    private String packageName;

    public enum WebRequestResult {OK, ACCESS_DENIED, NETWORK_ERROR}


    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        initServices(context);
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);

        initServices(context);
    }



    private void initServices(Context context){
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(SslUtils.getUnsafeOkHttpClient())
                .build();

        service = retrofit.create(ButtonWebService.class);
        ButtonDatabase db = ButtonDatabase.get(context);
        store=db.buttonPressStore();
        accountManager = AccountManager.get(context);
        packageSHA1 = SecurityUtils.getPackageSHA1(context);
        packageName = context.getPackageName();
    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String authority, ContentProviderClient contentProviderClient, SyncResult syncResult)  {
        String token = getToken(account);
        if(token!=null){
            List<ButtonPress> buttonPresses = store.selectUnUploaded();
            List<ButtonActionLog> buttonActionLogs = store.selectUnuploadedActionLogs();
            List<DeviceLog> deviceLogs = store.selectUnuploadedDeviceLogs();

            WebRequestResult webRequestResult;
            if(buttonPresses!=null && buttonPresses.size()>0) {
                webRequestResult = uploadButtonPresses(token,buttonPresses);
                if(webRequestResult==WebRequestResult.OK){
                    store.setUploadedButtonPresses(buttonPresses);
                }else {
                    syncResult.stats.numIoExceptions++;
                    if(webRequestResult== WebRequestResult.ACCESS_DENIED) invalidateToken(account, token);
                }
            }

            if(buttonActionLogs!=null && buttonActionLogs.size()>0) {
                webRequestResult = uploadButtonActionLogs(token,buttonActionLogs);
                if(webRequestResult==WebRequestResult.OK){
                    store.setUploadedButtonActionLogs(buttonActionLogs);
                }else {
                    syncResult.stats.numIoExceptions++;
                    if(webRequestResult== WebRequestResult.ACCESS_DENIED) invalidateToken(account, token);
                }
            }

            if(deviceLogs!=null && deviceLogs.size()>0) {
                webRequestResult = uploadDeviceLogs(token,deviceLogs);
                if(webRequestResult==WebRequestResult.OK){
                    store.setUploadedDeviceLogs(deviceLogs);
                }else {
                    syncResult.stats.numIoExceptions++;
                    if(webRequestResult== WebRequestResult.ACCESS_DENIED) invalidateToken(account, token);
                }
            }
        }
    }



    private String getToken(Account account){
        String token = null;
        AccountManagerFuture<Bundle> b;
        Bundle options = new Bundle();
        options.putString(KEY_SIGNATURE, packageSHA1);
        b=accountManager.getAuthToken(account,packageName,options,true,null,null);
        try {
            Bundle bundle = b.getResult(10, TimeUnit.SECONDS);
            if(b.isDone()) {
                token = bundle.getString(AccountManager.KEY_AUTHTOKEN, null);
            }
        }catch (OperationCanceledException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
        return token;
    }

    private void invalidateToken(Account account, String token) {
            accountManager.invalidateAuthToken(account.type, token);
    }

    private WebRequestResult uploadButtonPresses(String token, List<ButtonPress> buttonPresses) {
        Call<Void> call = service.addButtonPressList(token,buttonPresses);
        return executeCall(call);
    }

    private WebRequestResult uploadButtonActionLogs(String token, List<ButtonActionLog> buttonActionLogs) {
        Call<Void> call = service.addButtonActionLogList(token,buttonActionLogs);
        return executeCall(call);
    }

    private WebRequestResult uploadDeviceLogs(String token, List<DeviceLog> deviceLogs) {
        Call<Void> call = service.addDeviceLogList(token,deviceLogs);
        return executeCall(call);
    }

    private WebRequestResult executeCall(Call<Void> call){
        try {
            Response<Void> response = call.execute();
            if(response.isSuccessful()){
                return WebRequestResult.OK;
            }else{
                if(response.code()==403){
                    return WebRequestResult.ACCESS_DENIED;
                }else{
                    return WebRequestResult.NETWORK_ERROR;
                }
            }
        } catch (IOException e) {
            return WebRequestResult.NETWORK_ERROR;
        }
    }

//    private WebRequestResult uploadPressesOld(String token) {
//        List<ButtonPress> buttonPressList = store.selectUnUploaded();
//        Call<Void> call = service.addButtonPressList(token,buttonPressList);
//
//        try {
//            Response<Void> response = call.execute();
//            if(response.isSuccessful()){
//
//
//                for(ButtonPress buttonPress: buttonPressList) {
//                    ButtonPress press = new ButtonPress(
//                            buttonPress.id,
//                            buttonPress.date,
//                            buttonPress.buttonId,
//                            true,
//                            buttonPress.action,
//                            buttonPress.canceled
//
//                    );
//                    Log.d("happyUpdate", buttonPress.toString());
//                    store.update(press);
//                }
//                return WebRequestResult.OK;
//            }else{
//                if(response.code()==403){
//                    return WebRequestResult.ACCESS_DENIED;
//                }else{
//                    return WebRequestResult.NETWORK_ERROR;
//                }
//
//
//
//            }
//        } catch (IOException e) {
//            return WebRequestResult.NETWORK_ERROR;
//        }
//    }
}
