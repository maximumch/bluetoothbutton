package ru.nix.bluetoothbutton;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.nix.bluetoothbutton.dao.ButtonDatabase;
import ru.nix.bluetoothbutton.model.ButtonAction;
import ru.nix.bluetoothbutton.model.ButtonDevice;
import ru.nix.bluetoothbutton.model.ButtonPress;
import ru.nix.bluetoothbutton.model.ButtonStatistics;
import ru.nix.bluetoothbutton.model.ConnectionState;

public class Main2Activity extends AppCompatActivity {

    public static final int PERMISSIONS_REQUEST_CODE = 99;
    public static final int BLUETOOTH_ENABLE_REQUEST_CODE = 98;
    private TextView statusText;
    private ProgressBar progressBar;
    private Map<Integer,TextView> tns;
    private Map<Integer,TextView> acts;
    private State state;
    private View connectedDevice;
    private TextView deviceName;
    //private ProgressBar chargeLevelProgress;
    private ImageView chargeLevelImage;
    private TextView chargeLevelText;
    private TextView connectionState;
    private RecyclerView pressesList;
    private PressesAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        state=(State)getLastCustomNonConfigurationInstance();
        if(state==null) {
            state = new State();
        }
        statusText = findViewById(R.id.status_text);
        progressBar = findViewById(R.id.progress);
        connectedDevice = findViewById(R.id.connected_device);
        deviceName = findViewById(R.id.device_name);
        //chargeLevelProgress = findViewById(R.id.charge_level_progress);
        chargeLevelImage = findViewById(R.id.charge_level_image);
        chargeLevelText = findViewById(R.id.charge_level_text);
        connectionState= findViewById(R.id.connection_state);
        pressesList= findViewById(R.id.presses_list);
        adapter = new PressesAdapter();
        pressesList.setAdapter(adapter);
        pressesList.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));




       // setConnectedDevice();
        tns = new HashMap<>();
        tns.put(1,findViewById(R.id.n1));
        tns.put(2,findViewById(R.id.n2));
        tns.put(3,findViewById(R.id.n3));
        tns.put(4,findViewById(R.id.n4));
        tns.put(5,findViewById(R.id.n5));
        tns.put(6,findViewById(R.id.n6));
        tns.put(7,findViewById(R.id.n7));
        tns.put(8,findViewById(R.id.n8));
        tns.put(9,findViewById(R.id.n9));
        tns.put(10,findViewById(R.id.n10));

        acts = new HashMap<>();
        acts.put(1,findViewById(R.id.act1));
        acts.put(2,findViewById(R.id.act2));
        acts.put(3,findViewById(R.id.act3));
        acts.put(4,findViewById(R.id.act4));
        acts.put(5,findViewById(R.id.act5));
        acts.put(6,findViewById(R.id.act6));
        acts.put(7,findViewById(R.id.act7));
        acts.put(8,findViewById(R.id.act8));
        acts.put(9,findViewById(R.id.act9));
        acts.put(10,findViewById(R.id.act10));


        findViewById(R.id.soft1).setOnClickListener((view)->onSoftButtonClick(7));
        findViewById(R.id.soft2).setOnClickListener((view)->onSoftButtonClick(8));
        findViewById(R.id.soft3).setOnClickListener((view)->onSoftButtonClick(9));
        findViewById(R.id.soft4).setOnClickListener((view)->onSoftButtonClick(10));

        findViewById(R.id.hard1).setOnLongClickListener((view)->onButtonLongClick(1));
        findViewById(R.id.hard2).setOnLongClickListener((view)->onButtonLongClick(2));
        findViewById(R.id.hard3).setOnLongClickListener((view)->onButtonLongClick(3));
        findViewById(R.id.hard4).setOnLongClickListener((view)->onButtonLongClick(4));
        findViewById(R.id.hard5).setOnLongClickListener((view)->onButtonLongClick(5));
        findViewById(R.id.hard6).setOnLongClickListener((view)->onButtonLongClick(6));
        findViewById(R.id.soft1).setOnLongClickListener((view)->onButtonLongClick(7));
        findViewById(R.id.soft2).setOnLongClickListener((view)->onButtonLongClick(8));
        findViewById(R.id.soft3).setOnLongClickListener((view)->onButtonLongClick(9));
        findViewById(R.id.soft4).setOnLongClickListener((view)->onButtonLongClick(10));


        //BluetoothService.BUSDevices.observe(this, event -> onBluetoothServiceEvent(event));
        ButtonDatabase.get(this).buttonPressStore().selectAllStatisticsLD().observe(this, statistics -> onStatisticsChanged(statistics));
        ButtonDatabase.get(this).buttonPressStore().selectLastConnectedDevicesLD().observe(this, devices -> setConnectedDevice(devices));
        ButtonDatabase.get(this).buttonPressStore().selectAllActionsLD().observe(this, actions -> onActionsChanged(actions));
        ButtonDatabase.get(this).buttonPressStore().selectTopPressesLD().observe(this, presses -> onPressesChanged(presses));

        checkPermissions();
        checkBluetoothEnabled();

    }



    @Override
    protected void onResume() {
        super.onResume();

    }




    private void setConnectedDevice(List<ButtonDevice> devices) {
        if(devices!=null && devices.size()>0){
            deviceName.setText(devices.get(0).name);
            updateChargeLevelImageAndText((int) devices.get(0).charge, devices.get(0).usbConnected,devices.get(0).lastChargeReceived);
            if(devices.get(0).connectionState== ConnectionState.DISCONNECTED) {
                connectionState.setText(R.string.connection_state_disconnected);
                progressBar.setVisibility(View.INVISIBLE);
            }else if(devices.get(0).connectionState== ConnectionState.CONNECTING){
                connectionState.setText(R.string.connection_state_connecting);
                progressBar.setVisibility(View.VISIBLE);
            }else if(devices.get(0).connectionState== ConnectionState.CONNECTED) {
                connectionState.setText(R.string.connection_state_connected);
                progressBar.setVisibility(View.INVISIBLE);
            }


        }
    }

    private void updateChargeLevelImageAndText(int chargeLevel, boolean usbConnected, long lastChargeReceived){
        int image_res_id;
        String text;

        if(System.currentTimeMillis()-lastChargeReceived > 10*60*1000){
            image_res_id = R.drawable.ic_battery_unknown;
            text = "";

        }else {
            text = chargeLevel + " %";
            if (chargeLevel > 95) {
                if (usbConnected) {
                    image_res_id = R.drawable.ic_battery_charging_full;
                } else {
                    image_res_id = R.drawable.ic_battery_full;
                }
            } else if (chargeLevel > 85) {
                if (usbConnected) {
                    image_res_id = R.drawable.ic_battery_charging_90;
                } else {
                    image_res_id = R.drawable.ic_battery_90;
                }
            } else if (chargeLevel > 70) {
                if (usbConnected) {
                    image_res_id = R.drawable.ic_battery_charging_80;
                } else {
                    image_res_id = R.drawable.ic_battery_80;
                }
            } else if (chargeLevel > 55) {
                if (usbConnected) {
                    image_res_id = R.drawable.ic_battery_charging_60;
                } else {
                    image_res_id = R.drawable.ic_battery_60;
                }
            } else if (chargeLevel > 40) {
                if (usbConnected) {
                    image_res_id = R.drawable.ic_battery_charging_50;
                } else {
                    image_res_id = R.drawable.ic_battery_50;
                }
            } else if (chargeLevel > 25) {
                if (usbConnected) {
                    image_res_id = R.drawable.ic_battery_charging_30;
                } else {
                    image_res_id = R.drawable.ic_battery_30;
                }
            } else {
                if (usbConnected) {
                    image_res_id = R.drawable.ic_battery_charging_20;
                } else {
                    image_res_id = R.drawable.ic_battery_20;
                }
            }
        }
        chargeLevelImage.setImageResource(image_res_id);
        chargeLevelText.setText(text);

    }


    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return(state);
    }

    private void onStatisticsChanged(@Nullable List<ButtonStatistics> buttonStatistics) {
        for(ButtonStatistics statistics: buttonStatistics){
            TextView tn = tns.get(statistics.buttonId);
            if(tn!=null) tn.setText(String.valueOf(statistics.pressesNumber));

        }
    }

    private void onActionsChanged(@Nullable List<ButtonAction> buttonActions) {
        for(ButtonAction action: buttonActions){
            TextView act = acts.get(action.buttonId);
            if(act!=null) {


                String actionString;
                int textColor;
                if(action.cancelPrevious){
                    textColor = getResources().getColor(R.color.colorAccent);
                    actionString = getString(R.string.cancel);
                }else {
                    textColor = getResources().getColor(R.color.colorPrimary);
                    if (action.action != null) {
                        actionString = action.action;
                    } else {
                        actionString = getString(R.string.button1_name_prefix) + " " + action.buttonId;
                        //actionString = "" + action.buttonId;
                    }
                }
                act.setText(actionString);
                act.setTextColor(textColor);

            }
        }
    }

    private void onPressesChanged(@Nullable List<ButtonPress> buttonPresses) {
        if(buttonPresses!=null){
            for(ButtonPress press: buttonPresses){
                Log.d("happyData", press.toString());
            }




            adapter.presses = buttonPresses;
        }else{
            adapter.presses = new ArrayList<>();
        }

        adapter.notifyDataSetChanged();


//        int i=0;
//        int j=0;
//        ButtonPress press1 = null;
//        ButtonPress press2 = null;
//
//
//        while(adapter.presses.size()>i || buttonPresses.size()>j) {
//            if (adapter.presses.size() > i) {
//                press1 = adapter.presses.get(i);
//            }else{
//                press1 = null;
//            }
//            if (buttonPresses.size() > j){
//                press2 = adapter.presses.get(j);
//            }else{
//                press2 = null;
//            }
//
//            if (press1 == null && press2 != null) {
//                adapter.presses.add(i,press2);
//                adapter.notifyItemInserted(i);
//                i++;
//                j++;
//            }else if(press1 != null && press2 == null){
//                adapter.presses.remove(i);
//                adapter.notifyItemRemoved(i);
//            }else if(press1 != null && press2 != null){
//
//
//
//            }
//
//
//
//
//
//
//
//        }
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//        int n=adapter.presses.size();
//        if(n>0) {
//            adapter.presses.clear();
//            adapter.notifyItemRangeRemoved(0,n);
//        }
//
//        if(buttonPresses!=null && buttonPresses.size()>0) {
//            adapter.presses.addAll(buttonPresses);
//            adapter.notifyItemRangeInserted(0, buttonPresses.size() - 1);
//        }
    }

    private void onSoftButtonClick(int buttonId){
        ButtonDatabase.get(this).buttonPressStore().addButtonPress(new ButtonPress(buttonId));
    }

    private boolean onButtonLongClick(int buttonId){
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(500);
        Intent intent = new Intent(this,ActionActivity.class);
        intent.putExtra(ActionActivity.EXTRA_BUTTON_ID, buttonId);
        startActivity(intent);
        return true;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main2,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, BluetoothService.class);

        switch(item.getItemId()){
            case R.id.startScan:
                intent.putExtra(BluetoothService.EXTRA_ACTION,BluetoothService.ACTION_START_SCAN);
                startService(intent);
                Intent activityIntent = new Intent(this,ButtonsScanActivity.class);
                startActivity(activityIntent);
                return true;
            case R.id.disconnect:
                intent.putExtra(BluetoothService.EXTRA_ACTION,BluetoothService.ACTION_DISCONNECT);
                startService(intent);
                return true;
            case R.id.reset_counters:
                resetCounters();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void resetCounters(){
        showAlertDialog(R.string.reset_counters_dialog_title, R.string.reset_counters_dialog_message, R.string.reset_counters_dialog_positive_button,
                R.string.reset_counters_negative_button, (DialogInterface dialogInterface, int i) -> {
                    ButtonDatabase.get(this).buttonPressStore().resetButtonStatistics();
                    dialogInterface.cancel();
                });
    }

    private void checkBluetoothEnabled(){
        BluetoothManager manager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter mBluetoothAdapter = manager.getAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, BLUETOOTH_ENABLE_REQUEST_CODE);
        }
    }



    @TargetApi(23)
    private void checkPermissions() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            try {
                List<String> permissionsToRequest = new ArrayList<>();
                PackageManager pm = this.getPackageManager();
                PackageInfo info = pm.getPackageInfo(this.getPackageName(),PackageManager.GET_PERMISSIONS);
                String[] permissions = info.requestedPermissions;
                for(String permission: permissions){
                    if(checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED){
                        permissionsToRequest.add(permission);
                    }
                }
                if(permissionsToRequest.size()>0) {
                    String[] p = new String[]{};
                    p = permissionsToRequest.toArray(p);
                    requestPermissions(p, PERMISSIONS_REQUEST_CODE);
                }
            } catch (PackageManager.NameNotFoundException e) {

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==PERMISSIONS_REQUEST_CODE){
            for(int grantResult: grantResults){
                if(grantResult!=PackageManager.PERMISSION_GRANTED){
                    showPermissionDeniedDialog();
                    return;
                }
            }
        }else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==BLUETOOTH_ENABLE_REQUEST_CODE){
            if(resultCode != -1) {
                showBlutoothDisabledDialog();
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void showPermissionDeniedDialog() {
        showAlertDialog(R.string.permission_denied_dialog_title, R.string.permission_denied_dialog_message, R.string.permission_denied_dialog_neutral_button);
    }

    private void showBlutoothDisabledDialog() {
        showAlertDialog(R.string.bluetooth_disabled_dialog_title, R.string.bluetooth_disabled_dialog_message, R.string.bluetooth_disabled_dialog_neutral_button);
    }

    private void showAlertDialog(int titleId, int messageId, int buttonLabelId) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(titleId)
                .setMessage(messageId)
                .setPositiveButton(buttonLabelId, (DialogInterface dialogInterface, int i) -> {
                        dialogInterface.cancel();
                    }
                )
                .create();
        dialog.show();
    }

    private void showAlertDialog(int titleId, int messageId, int positiveButtonLabelId, int negativeButtonLabelId,
                                 DialogInterface.OnClickListener positiveButtonListener) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(titleId)
                .setMessage(messageId)
                .setNegativeButton(negativeButtonLabelId,(DialogInterface dialogInterface, int i) -> {
                    dialogInterface.cancel();
                })
                .setPositiveButton(positiveButtonLabelId, positiveButtonListener)
                .create();
        dialog.show();
    }



    private static class State {
        final ArrayList<BluetoothService.Event> events=new ArrayList<>();
        List<ButtonPress> presses;

    }

    private class PressesAdapter extends RecyclerView.Adapter<PressesHolder>{
        List<ButtonPress> presses=new ArrayList<>();

        @Override
        public PressesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View deviceView=
                    getLayoutInflater()
                            .inflate(R.layout.press_item_new, parent, false);


            return(new PressesHolder(deviceView));
        }

        @Override
        public void onBindViewHolder(PressesHolder holder, int position) {
            holder.bind(presses.get(position));
        }

        @Override
        public int getItemCount() {
            return presses.size();
        }
    }



    private static class PressesHolder extends RecyclerView.ViewHolder {
        private final TextView pressAction;
        private final TextView pressDate;
        //private final TextView pressButtonNumber;
        private ImageView syncStatusImage;




        public PressesHolder(View itemView) {
            super(itemView);
            pressAction = itemView.findViewById(R.id.press_action);
            pressDate = itemView.findViewById(R.id.press_date);
            //pressButtonNumber = itemView.findViewById(R.id.press_button_number);
            syncStatusImage = itemView.findViewById(R.id.sync_image);
        }


        void bind(ButtonPress press) {
            String buttonNumberString = this.itemView.getContext().getString(R.string.button1_name_prefix) + " " + press.buttonId;

            if(press.action==null){
                pressAction.setText(buttonNumberString);
            }else{
                pressAction.setText( press.action);
            }

            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
            pressDate.setText(format.format(press.date));
            if(press.uploaded){
                syncStatusImage.setVisibility(View.VISIBLE);
            }else{
                syncStatusImage.setVisibility(View.INVISIBLE);
            }



            //pressButtonNumber.setText(buttonNumberString);





//            address.setText(device.address);
//            name.setText(device.name);
//            buttonDevice = device;
//            if(device.lastConnected>0){
//                lastConnected.setImageResource(R.drawable.ic_bluetooth_connected_green_24dp);
//            }else{
//                lastConnected.setImageResource(R.drawable.ic_bluetooth_connected_light_24dp);
//            }


        }



    }


}
